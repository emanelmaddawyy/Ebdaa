<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ebdaa');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{TuK!`-K3+PZE)cx+w&q|bAojWuf/wf!#.F=yFx^))m,/NF9YK1I^0/caqi{I+F:');
define('SECURE_AUTH_KEY',  '7La4v]bLbB8FLxD~8`s9a_507uHa}pUAH~KFVSh6rgFRK5)n6[C[-j057r6.QOXx');
define('LOGGED_IN_KEY',    '[qku_c{hqa,$%1Z;yK6)Y.Dvf4p{GiL88=7Qj6p][:r0Ta0OZ5PM8/&s<9?>#cMX');
define('NONCE_KEY',        'Eu#spiVZG0EW6h>u[K-s}{44M=IZtl*]&..n ?69K+JcD5IzB8pM`S:,rHC3GFl/');
define('AUTH_SALT',        '%1gO`Bu$KJz](lU20*a9o<-~Fj 1dWPq:Nk,FiJe:tv4DQN:vd0knb%+/pB#(p,a');
define('SECURE_AUTH_SALT', 'V.[_fG>*>g&`0@0k^e.(05_@{Mu&|h<<x(89/07o2(eVwp5|6Bej.JU)yRXwCSkw');
define('LOGGED_IN_SALT',   ':UI])xG6K*u]NVjm?NrZ-W f/Ib(#&d;9Tov0fBk%TrSo{!:6w|d2VT)u|VSpX* ');
define('NONCE_SALT',       '^:gQD:>PfG$,)te2Z?3+|R8,UDOqsW5Z|2dj;%H_)ugk35X {mj<Rt(K-w5b^Xog');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
