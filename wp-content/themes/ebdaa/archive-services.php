<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Page Title Start -->
<div class="page-title-area about-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">Services</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="#">Services</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<div class="service">
	<div class="container">
		<div class="row">
			<div class="box">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post();?>
						<?php
                $image_id = get_post_thumbnail_id();
                $image_size = 'services';
                $image_attachment = wp_get_attachment_image_src( $image_id, $image_size );
                $image_url = $image_attachment[0];
						?>
						<div class="col-md-4 col-sm-6 inner">
							<div class="single-service">
								<a href="<?php the_permalink(); ?>">
									<?php if( $image_url ) { ?>
									<img src="<?php echo $image_url; ?>" alt=""/>
									<?php } else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service-default.jpg" alt=""/>
									<?php } ?>
								</a>
								<div class="service-icon">
								<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
									<p class="description"><?php the_field('description'); ?></p>
									<p><a href="<?php the_permalink(); ?>">Read More</a></p>
								</div>
							</div>
						</div>
					<?php endwhile;?>
				 <?php else :
								 get_template_part( 'template-parts/post/content', 'none' );
				 endif; ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer();
