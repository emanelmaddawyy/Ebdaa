<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Slider Start -->
		<div class="main-slider">
			<?php if( have_rows('slider') ): ?>

	<div class="slide-carousel owl-item">

	<?php while( have_rows('slider') ): the_row();

		// vars
		$image = get_sub_field('image');
		$title = get_sub_field('title');
		$sub_title = get_sub_field('sub_title');
		$link_text = get_sub_field('link_text');
		$link = get_sub_field('link');

		?>
		<div class="item" style="background-image:url(<?php echo $image['url']; ?>);">
			<div class="overlay"></div>
			<div class="text">
					<div class="inner-text">
						<div class="this-item">
							<?php if( $title ): ?>
								<h2><?php echo $title; ?></h2>
							<?php endif; ?>
						</div>
						<div class="this-item">
							<?php if( $sub_title ): ?>
								<h3><?php echo $sub_title; ?></h3>
							<?php endif; ?>
						</div>
						<?php if( $link ): ?>
							<div class="this-item readmore-buttons">
									<p><a href="<?php echo $link; ?>"><?php echo $link_text; ?></a></p>
							</div>
						<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
	<?php endif; ?>
		</div>
		<!-- Slider End -->

		<section class="about">
			<div class="container">
				<div class="row">
					<div class="title">
						<h1>About Us</h1>
						<div class="shape-border"><i class="fa fa-home"></i></div>
						<p>Welcome to our Construction Related Services</p>
					</div>
					<?php $pages = get_pages(array(
						'meta_key' => '_wp_page_template',
						'meta_value' => 'page-about.php'
						));
						foreach($pages as $page) :
					?>
					<?php if( get_field('brochure', $page->ID) ): ?>
						<ul>
							<li><a href="<?php the_field('brochure', $page->ID); ?>" target="_blank">Brochure</a></li>
						</ul>
					<?php endif; ?>
					<?php endforeach; ?>
					<div class="col-md-4 col-sm-4">
						<div class="about-text">
							<h3>Who We Are</h3>
							<?php the_field('who_we_are', $page->ID); ?>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="about-text">
							<h3>Ebdaa Mission</h3>
							<?php the_field('ebdaa_mission', $page->ID); ?>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="about-text">
							<h3>Ebdaa Vission</h3>
							<?php the_field('ebdaa_vission', $page->ID); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Service Start -->
		<div class="service bg-gray">
			<div class="container">
				<div class="row">
					<div class="title">
						<h1>Our Service</h1>
						<div class="shape-border"><i class="fa fa-home"></i></div>
						<p>Check All Our Important and Latest Service From Below</p>
					</div>
					<div class="box">
						<?php $args = array('posts_per_page' => 6, 'post_type' => 'services', 'meta_query' => array(
	            array(
                'key' => 'featured',
                'compare' => '==',
                'value' => 1
								)
							));?>
              <?php $wp_query = new WP_Query($args); ?>
	              <?php if ($wp_query->have_posts()) : ?>
									<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
										<?php
				                $image_id = get_post_thumbnail_id();
				                $image_size = 'services';
				                $image_attachment = wp_get_attachment_image_src( $image_id, $image_size );
				                $image_url = $image_attachment[0];
										?>
										<div class="col-md-4 col-sm-6 inner">
											<div class="single-service">
												<?php if( $image_url ) { ?>
												<img src="<?php echo $image_url; ?>" alt=""/>
												<?php } else { ?>
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service-default.jpg" alt=""/>
												<?php } ?>
												<div class="service-icon">
												<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
													<p class="description"><?php the_field('description'); ?></p>
													<p><a href="<?php the_permalink(); ?>">Read More</a></p>
												</div>
											</div>
										</div>
									<?php endwhile; ?>
								<?php endif; ?>
							<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- Service End -->
		<!-- Call To Actions Start -->
		<div class="call-to-action-area">
			<div class="images-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="call-to-action">
							<div class="call-to-action-text">
								<h2>Join EBDAA Team Now!!</h2>
								<p></p>
							</div>
							<div class="call-to-action-button">
								<ul>
									<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>/careers">CAREERS</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- Call To Actions End -->

		<!-- Project Start -->
		<div class="project-area">
			<div class="container">
				<div class="row">
					<div class="title">
						<h1>Projects</h1>
						<div class="shape-border"><i class="fa fa-home"></i></div>
						<p>We have many projects listed bellow</p>
					</div>
					<?php $args = array('posts_per_page' => 6, 'post_type' => 'projects', 'meta_query' => array(
						array(
							'key' => 'featured',
							'compare' => '==',
							'value' => 1
							)
						));?>
						<?php $wp_query = new WP_Query($args); ?>
							<?php if ($wp_query->have_posts()) : ?>
								<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
									<?php
											$image_id = get_post_thumbnail_id();
											$image_size = 'projects';
											$image_attachment = wp_get_attachment_image_src( $image_id, $image_size );
											$image_url = $image_attachment[0];
									?>
									<div class="col-md-4 col-sm-6">
										<div class="single-practices">
											<div class="single-project-thumb">
												<?php if( $image_url ) { ?>
												<img src="<?php echo $image_url; ?>" alt=""/>
												<?php } else { ?>
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/project-default.jpg" alt=""/>
												<?php } ?>
											</div>
											<div class="project-content">
												<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
												<div class="project-meta">
													<ul>
														<li><i class="fa fa-calendar"></i><?php the_time('F Y')  ?></li>
													</ul>
												</div>
												<a href="<?php the_permalink(); ?>" class="readmore-button">Read More</a>
											</div>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						<?php wp_reset_query(); ?>
				</div>
			</div>
		</div>
		<!-- Project End -->

<?php get_footer();
