<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Slider Start -->
		<div class="main-slider">
			<div class="slide-carousel owl-item">
				<div class="item" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/slider1.jpg);">
					<div class="overlay"></div>
					<div class="text">
					    <div class="inner-text">
                            <div class="this-item">
                                <h2>Welcome To Our Company</h2>
                            </div>
                            <div class="this-item">
                                <h3>We Provide Fast And Reliable Service to Clients</h3>
                            </div>
                            <div class="this-item readmore-buttons">
                                <p><a href="about.html">About us</a></p>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Slider End -->

		<section class="about">
			<div class="container">
				<div class="row">
					<div class="title">
						<h1>About Us</h1>
						<div class="shape-border"><i class="fa fa-home"></i></div>
						<p>Welcome to our Construction Related Services</p>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="about-text">
							<h3>Our Mission</h3>
							<p>
								Through using our innovative employees, We provide the highest  level of commitment to our clients by oroviding the highest quality, productivity, at proper time with competitive prices , we target the client satisfaction.
							</p>
							<p>
								Our Professional Employees are Our Assets .
							</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="about-text">
							<h3>Our Vision</h3>
							<p>
							A multiple discipline engineering company is our target which covering each and every trade all over the world.
							</p>

						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="about-img">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/g12.jpg" alt="about image">
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Service Start -->
		<div class="service bg-gray">
			<div class="container">
				<div class="row">
					<div class="title">
						<h1>Our Service</h1>
						<div class="shape-border"><i class="fa fa-home"></i></div>
						<p>Check All Our Important and Latest Service From Below</p>
					</div>
					<div class="box">
						<div class="col-md-4 col-sm-6 inner">
							<div class="single-service">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/event2.jpg" alt=""/>
								<div class="service-icon">
									<h3><a href="service-details.html">Management Construction</a></h3>
									<p>Lorem ipsum dolor sit amet, maiorum phaedrum molestiae qui cu, ex vivendo invidunt tincidunt vix, dicant fabellas per eu. Ex his inermis.</p>
									<p><a href="service-details.html">Read More</a></p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 inner">
							<div class="single-service">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/event1.jpg" alt=""/>
								<div class="service-icon">
									<h3><a href="service-details.html">Modern Design</a></h3>
									<p>Lorem ipsum dolor sit amet, maiorum phaedrum molestiae qui cu, ex vivendo invidunt tincidunt vix, dicant fabellas per eu. Ex his inermis.</p>
									<p><a href="service-details.html">Read More</a></p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 inner">
							<div class="single-service">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/event3.jpg" alt=""/>
								<div class="service-icon">
									<h3><a href="service-details.html">Repair service</a></h3>
									<p>Lorem ipsum dolor sit amet, maiorum phaedrum molestiae qui cu, ex vivendo invidunt tincidunt vix, dicant fabellas per eu. Ex his inermis.</p>
									<p><a href="service-details.html">Read More</a></p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 inner">
							<div class="single-service">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/event4.jpg" alt=""/>
								<div class="service-icon">
									<h3><a href="service-details.html">Electrical Service</a></h3>
									<p>Lorem ipsum dolor sit amet, maiorum phaedrum molestiae qui cu, ex vivendo invidunt tincidunt vix, dicant fabellas per eu. Ex his inermis.</p>
									<p><a href="service-details.html">Read More</a></p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 inner">
							<div class="single-service">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/event5.jpg" alt=""/>
								<div class="service-icon">
									<h3><a href="service-details.html">Creative Plumbing</a></h3>
									<p>Lorem ipsum dolor sit amet, maiorum phaedrum molestiae qui cu, ex vivendo invidunt tincidunt vix, dicant fabellas per eu. Ex his inermis.</p>
									<p><a href="service-details.html">Read More</a></p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 inner">
							<div class="single-service">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/event6.jpg" alt=""/>
								<div class="service-icon">
									<h3><a href="service-details.html">Consultant Service</a></h3>
									<p>Lorem ipsum dolor sit amet, maiorum phaedrum molestiae qui cu, ex vivendo invidunt tincidunt vix, dicant fabellas per eu. Ex his inermis.</p>
									<p><a href="service-details.html">Read More</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Service End -->
		<!-- Call To Actions Start -->
		<div class="call-to-action-area">
			<div class="images-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="call-to-action">
							<div class="call-to-action-text">
								<h2>DO YOU FACE ANY PROBLEM? CONTACT US NOW!!!</h2>
								<p>If you have any legal problem in your life, we are here to help you! </p>
							</div>
							<div class="call-to-action-button">
								<ul>
									<li><a href="contact.html">CONTACT US</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- Call To Actions End -->

		<!-- Project Start -->
		<div class="project-area">
			<div class="container">
				<div class="row">
					<div class="title">
						<h1>Complete Project</h1>
						<div class="shape-border"><i class="fa fa-home"></i></div>
						<p>We have complete many projects listed bellow</p>
					</div>
					<div class="col-md-4 col-sm-4 ">
						<div class="single-practices">
							<div class="single-project-thumb" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/g1.jpg)">
							</div>
							<div class="project-content">
								<h3><a href="service-details.html">Project One</a></h3>
								<div class="project-meta">
									<ul>
										<li><a href="#"><i class="fa fa-calendar"></i>28 April, 2017</a></li>
										<li><a href="#"><i class="fa fa-clock-o"></i>15.35</a></li>
									</ul>
								</div>
								<a href="project-details.html" class="readmore-button">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 ">
						<div class="single-practices">
							<div class="single-project-thumb" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/g1.jpg)">
							</div>
							<div class="project-content">
								<h3><a href="service-details.html">Project Two</a></h3>
								<div class="project-meta">
									<ul>
										<li><a href="#"><i class="fa fa-calendar"></i>28 April, 2017</a></li>
										<li><a href="#"><i class="fa fa-clock-o"></i>15.35</a></li>
									</ul>
								</div>
								<a href="project-details.html" class="readmore-button">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 ">
						<div class="single-practices">
							<div class="single-project-thumb" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/g3.jpg)">
							</div>
							<div class="project-content">
								<h3><a href="service-details.html">Project Three</a></h3>
								<div class="project-meta">
									<ul>
										<li><a href="#"><i class="fa fa-calendar"></i>28 April, 2017</a></li>
										<li><a href="#"><i class="fa fa-clock-o"></i>15.35</a></li>
									</ul>
								</div>
								<a href="project-details.html" class="readmore-button">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Project End -->

<?php get_footer();
