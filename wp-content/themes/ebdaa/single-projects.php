<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
/* Start the Loop */
while ( have_posts() ) : the_post();?>
<!-- Page Title Start -->
<div class="page-title-area single-course1-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<span class="page-title">Project Details</span>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>projects">Projects</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<!-- Single Course Start -->
<div class="course-single-page">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-12">
				<div class="course-single-img">
					<?php
							$image_id = get_post_thumbnail_id();
							$image_size = 'projects';
							$image_attachment = wp_get_attachment_image_src( $image_id, $image_size );
							$image_url = $image_attachment[0];
					?>
					<?php if( $image_url ) { ?>
					<img src="<?php echo $image_url; ?>" alt=""/>
					<?php } else { ?>
					<img class="default" src="<?php echo get_template_directory_uri(); ?>/assets/img/project-default2.jpg" alt=""/>
					<?php } ?>
				</div>
			</div>
			<div class="col-md-7 col-sm-12">
				<div class="course-single">
					<h2><?php echo get_the_title(); ?></h2>
					<div class="course-single-meta">
					</div>
					<?php echo get_the_content(); ?>
				</div>
			</div>
			<div class="course-details-area">
				<div class="tab-content">
					<div class="tab-pane active" id="Gallery">
						<?php
						$images = get_field('gallery');
						if( $images ): ?>
						<div class="title">
							<h1>Our Gallery</h1>
							<div class="shape-border"><i class="fa fa-home"></i></div>
						</div>
		        <?php foreach( $images as $image ): ?>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="course-gallery gallery-item margin-bottom-30">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<div class="gallery-img-overlay"></div>
									<div class="gallery-overlay">
										<div class="gallery-item-text">
											<h3><?php echo $image['alt']; ?></h3>
											<a class="gallery-photo" href="<?php echo $image['url']; ?>"><i class="fa fa-search-plus"></i></a>
										</div>
									</div>
								</div>
							</div>
		        <?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Single Course End -->
<?php endwhile; // End of the loop.
?>
<?php get_footer();
