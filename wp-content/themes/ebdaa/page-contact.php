<?php
/**
 *Template Name: Contact
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<!-- Page Title Start -->
<div class="page-title-area contact-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">Contact Us</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->
<!-- Contact Start -->
<section class="contact">
	<div class="container">
		<div class="row">

			<div class="col-md-4 inner">
				<div class="single-contactinfo">
					<div class="contact-icon">
					<i class="fa fa-map-marker"></i>
					<h3>Egypt Office</h3>
						 <p><?php the_field('location'); ?></p>
					<div class="gap-10"></div>
					<h4><?php the_field('email'); ?></h4>
												 <h4><?php the_field('telephone'); ?></h4>
								 <h4><?php the_field('telephone2'); ?></h4>
					</div>
				</div>
			</div>
									<div class="col-md-8">
				<div class="google-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1726.7599393325843!2d31.306679419828928!3d30.050629214233545!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1530524073392" width="100%" height="450" allowfullscreen></iframe>
				</div>
			</div>

		</div>
		<div class="row">

			<div class="col-md-12 contactfrom">
				<div class="title">
					<h1>CONTACT FORM</h1>
					<div class="shape-border"><i class="fa fa-home"></i></div>
					<p>Fill up the form below to contact us and send us an email</p>
				</div>
				<div class="form-horizontal cform-1" >
					<?php echo do_shortcode( '[contact-form-7 id="25" title="Contact form"]' ); ?>
				</div>
			</div>

		</div>


	</div>
</section>
<!-- Contact End -->

<?php get_footer();
