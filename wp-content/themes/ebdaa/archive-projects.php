<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Page Title Start -->
<div class="page-title-area project-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">Projects</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="#">Projects</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<!-- Project Start -->
<div class="project-area">
	<div class="container">
		<div class="row">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post();?>
					<?php
							$image_id = get_post_thumbnail_id();
							$image_size = 'projects';
							$image_attachment = wp_get_attachment_image_src( $image_id, $image_size );
							$image_url = $image_attachment[0];
					?>
					<div class="col-md-4 col-sm-6">
						<div class="single-practices">
							<div class="single-project-thumb">
								<?php if( $image_url ) { ?>
								<img src="<?php echo $image_url; ?>" alt=""/>
								<?php } else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/project-default.jpg" alt=""/>
								<?php } ?>
							</div>
							<div class="project-content">
								<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
								<div class="project-meta">
									<ul>
										<li><i class="fa fa-calendar"></i><?php the_time('F Y')  ?></li>
									</ul>
								</div>
								<a href="<?php the_permalink(); ?>" class="readmore-button">Read More</a>
							</div>
						</div>
					</div>
				<?php endwhile;?>
			 <?php else :
							 get_template_part( 'template-parts/post/content', 'none' );
			 endif; ?>
		</div>
	</div>
</div>
<!-- Project End -->

<?php get_footer();
