<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Page Title Start -->
<div class="page-title-area about-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">About Page</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="#">About Page</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<!-- About Start -->
<section class="about bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4">
			<div class="about-text">
					<h3>Our Mission</h3>
					<p>
						Through using our innovative employees, We provide the highest  level of commitment to our clients by oroviding the highest quality, productivity, at proper time with competitive prices , we target the client satisfaction.
					</p>
					<p>
						Our Professional Employees are Our Assets .
					</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
			<div class="about-text">
					<h3>Our Vision</h3>
					<p>
					A multiple discipline engineering company is our target which covering each and every trade all over the world.
					</p>

				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="about-img">
					<img src="img/g12.jpg" alt="about image">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- About End -->
<?php get_footer();
