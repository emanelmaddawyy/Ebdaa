<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<!-- Favicon -->
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png">
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Header Top Start -->
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="logo">
						<a  href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="">
						</a>
					</div>
				</div>
				<div class="col-md-9">
					<div class="header-top-contact">
						<?php $pages = get_pages(array(
							'meta_key' => '_wp_page_template',
							'meta_value' => 'page-contact.php'
							));
							foreach($pages as $page) :
						?>
						<?php if( get_field('telephone', $page->ID) ): ?>
						<div class="intro">
							<i class="fa fa-phone"></i>
							<span>
								<strong>Call Us</strong>
								<b><?php the_field('telephone', $page->ID); ?></b>
							</span>
						</div>
						<?php endif; ?>
						<?php if( get_field('email', $page->ID) ): ?>
						<div class="intro">
							<i class="fa fa-envelope"></i>
							<span>
								<strong>E-mail Us</strong>
								<b><?php the_field('email', $page->ID); ?></b>
							</span>
						</div>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Header Top End -->
	<!-- Header Start -->
	<header>
		<div id="sticky" class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12">
						<div class="nav-menu">
							<!-- Nav Start -->
							<?php wp_nav_menu( array(
								'theme_location' => 'top',
								'menu_id'        => 'top-menu',
							) ); ?>
							<?php $pages = get_pages(array(
								'meta_key' => '_wp_page_template',
								'meta_value' => 'page-contact.php'
								));
								foreach($pages as $page) :
							?>
							<?php if( get_field('brochure', $page->ID) ): ?>
								<ul>
									<li><a href="<?php the_field('brochure', $page->ID); ?>" target="_blank">Brochure</a></li>
								</ul>
							<?php endif; ?>
							<?php endforeach; ?>
							<!-- Nav End -->
						</div>
					</div>
					<div class="col-md-3 col-sm-12 top-socials">
						<div class="header-top-social">
							<ul>
								<?php $pages = get_pages(array(
									'meta_key' => '_wp_page_template',
									'meta_value' => 'page-contact.php'
									));
									foreach($pages as $page) :
								?>
								<?php if( get_field('facebook', $page->ID) ): ?>
										<li><a href="<?php the_field('facebook', $page->ID); ?>" target="_blank">
											<i class="fa fa-facebook"></i>
										</a></li>
								<?php endif; ?>
								<?php if( get_field('twitter', $page->ID) ): ?>
										<li><a href="<?php the_field('twitter', $page->ID); ?>" target="_blank">
											<i class="fa fa-twitter"></i>
										</a></li>
								<?php endif; ?>
								<?php if( get_field('linked_in', $page->ID) ): ?>
										<li><a href="<?php the_field('linked_in', $page->ID); ?>" target="_blank">
											<i class="fa fa-linkedin"></i>
										</a></li>
								<?php endif; ?>
								<?php if( get_field('google', $page->ID) ): ?>
										<li><a href="<?php the_field('google', $page->ID); ?>" target="_blank">
											<i class="fa fa-google-plus"></i>
										</a></li>
								<?php endif; ?>
								<?php if( get_field('youtube', $page->ID) ): ?>
										<li><a href="<?php the_field('youtube', $page->ID); ?>" target="_blank">
											<i class="fa fa-youtube"></i>
										</a></li>
								<?php endif; ?>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Mobile start -->
		<div class="mobile-menu-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mobile-menu">
							<nav id="mobile-nav">
								<!-- Nav Start -->
								<?php wp_nav_menu( array(
									'theme_location' => 'top',
									'menu_id'        => 'top-menu',
								) ); ?>
								<?php $pages = get_pages(array(
									'meta_key' => '_wp_page_template',
									'meta_value' => 'page-contact.php'
									));
									foreach($pages as $page) :
								?>
								<?php if( get_field('brochure', $page->ID) ): ?>
									<ul>
										<li><a href="<?php the_field('brochure', $page->ID); ?>" target="_blank">Download Brochure</a></li>
									</ul>
								<?php endif; ?>
								<?php endforeach; ?>
								<!-- Nav End -->
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Mobile End -->
	</header>

	<div class="site-content-contain">
		<div id="content" class="site-content">
