<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
/* Start the Loop */
while ( have_posts() ) : the_post();?>
<!-- Page Title Start -->
<div class="page-title-area about-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title"><?php echo get_the_title(); ?></span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>services">Services</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<!-- About Start -->
<section class="about">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-7">
				<div class="about-text">
					<?php echo get_the_content(); ?>
				</div>
			</div>
			<div class="col-sm-6 col-md-5">
				<?php
						$image_id = get_post_thumbnail_id();
						$image_size = 'services';
						$image_attachment = wp_get_attachment_image_src( $image_id, $image_size );
						$image_url = $image_attachment[0];
				?>
				<?php if( $image_url ) { ?>
				<img src="<?php echo $image_url; ?>" alt=""/>
				<?php } else { ?>
				<img class="default" src="<?php echo get_template_directory_uri(); ?>/assets/img/service-default.jpg" alt=""/>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<!-- About End -->
<?php endwhile; // End of the loop.
?>
<?php get_footer();
