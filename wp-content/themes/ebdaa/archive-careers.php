<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Page Title Start -->
<div class="page-title-area about-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">Careers</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="#">Careers</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<div class="careers">
	<div class="container">
		<div class="row">
			<div class="box">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post();?>
					<div class="single-career">
						<div class="service-icon">
							<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
								<p><?php the_field('description'); ?></p>
								<p><a href="<?php the_permalink(); ?>">Read More</a></p>
						</div>
					</div>
				<?php endwhile;?>
			 <?php else :
							 get_template_part( 'template-parts/post/content', 'none' );
			 endif; ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer();
