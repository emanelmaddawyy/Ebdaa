<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->
		<!-- Footer Start-->
		<footer>
			<div class="footer">
				<div class="container">
					<div class="row">
						<?php $pages = get_pages(array(
							'meta_key' => '_wp_page_template',
							'meta_value' => 'page-contact.php'
							));
							foreach($pages as $page) :
						?>
						<div class="col-md-4 col-sm-4">
							<div class="contact-info-icon">
								<i class="fa fa-envelope-o"></i>
							</div>
							<div class="contact-info">
								<h2>Email</h2>
								<div class="contactsingle-info">
									<p><?php the_field('email', $page->ID); ?></p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="contact-info-icon">
								<i class="fa fa-phone"></i>
							</div>
							<div class="contact-info">
								<h2>Phone</h2>
								<div class="contactsingle-info">
									<p><?php the_field('telephone', $page->ID); ?></p>
									<p><?php the_field('telephone2', $page->ID); ?></p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="contact-info-icon">
								<i class="fa fa-map-marker"></i>
							</div>
							<div class="contact-info">
								<h2>Address</h2>
								<div class="contactsingle-info">
									<p><?php the_field('location', $page->ID); ?></p>
								</div>
							</div>
						</div>

						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<!-- Footer Copyright Area Start-->
			<div class="footer-copyright-area">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="footer-copyright">
								<p>Copyright &copy; 2018,E-Trends. All Right Reserved.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Footer Copyright Area End-->
		</footer>
		<!-- Footer End-->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>
<!-- Scroll to Top Start -->
<a href="#" class="scrollup">
	<i class="fa fa-angle-up"></i>
</a>
<!-- Scroll to Top End -->

<!-- Scripts Js Start -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.animate.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/waypoints.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.meanmenu.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>
<!-- Scripts Js End -->
</body>
</html>
