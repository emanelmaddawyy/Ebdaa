<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Page Title Start -->
<div class="page-title-area event-page" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/banner.jpg)">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">404 Page</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="#">404 Page</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<!-- 404 Error Start -->
<section class="error-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="error-title">
					<div class="inner">
						<span>404</span>
						<span>Error</span>
					</div>
				</div>
				<div class="content">
					<h2>404: Page Not Found</h2>
					<p>
						Oops! The page you were looking for doesn't exist.
						This might be happened, because you have typed the web address incorrectly.
					</p>
					<p class="button">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">Back to Home</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- 404 Error End -->

<?php get_footer();
