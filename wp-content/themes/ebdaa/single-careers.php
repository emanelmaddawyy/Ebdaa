<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
/* Start the Loop */
while ( have_posts() ) : the_post();?>
<!-- Page Title Start -->
<div class="page-title-area about-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">Careers</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>careers">Careers</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<!-- About Start -->
<section class="contact">
	<div class="container">
		<h3><?php echo get_the_title(); ?></h3>
		<p><?php echo get_the_content(); ?></p>
			<div class="col-md-12 contactfrom">
				<div class="title">
					<h1>CAREERS FORM</h1>
					<div class="shape-border"><i class="fa fa-home"></i></div>
					<p>Fill up the form below to apply this career</p>
				</div>
				<div class="form-horizontal cform-1" >
					<?php echo do_shortcode( '[contact-form-7 id="145" title="Careers form"]' ); ?>
				</div>
			</div>
	</div>
</section>
<!-- About End -->
<?php endwhile; // End of the loop.
?>
<?php get_footer();
