<?php
/**
 *Template Name: About
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<!-- Page Title Start -->
<div class="page-title-area about-page">
	<div class="image-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span class="page-title">About Us</span>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="breadcumb">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="#">About Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page Title End -->

<!-- About Start -->
<section class="about">
	<div class="container">
		<div class="about-single">
			<h3>Who We Are</h3>
			<div class="about-text"><?php the_field('who_we_are'); ?></div>
		</div>
		<div class="about-single">
			<h3>What We Do</h3>
			<div class="about-text"><?php the_field('what_we_do'); ?></div>
		</div>
		<div class="about-single">
			<h3>Why Ebdaa</h3>
			<div class="about-text"><?php the_field('why_ebdaa'); ?></div>
		</div>
		<div class="about-single">
			<h3>Ebdaa Vission</h3>
			<div class="about-text"><?php the_field('ebdaa_vission'); ?></div>
		</div>
		<div class="about-single">
			<h3>Ebdaa Mission</h3>
			<div class="about-text"><?php the_field('ebdaa_mission'); ?></div>
		</div>
	</div>
</section>
<!-- About End -->

<?php get_footer();
