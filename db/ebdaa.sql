-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2018 at 10:40 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebdaa`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-06-06 10:09:56', '2018-06-06 10:09:56', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/ebdaa', 'yes'),
(2, 'home', 'http://localhost/ebdaa', 'yes'),
(3, 'blogname', 'EBDAA', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'm.rohouma1@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:156:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"projects/?$\";s:28:\"index.php?post_type=projects\";s:41:\"projects/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=projects&feed=$matches[1]\";s:36:\"projects/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=projects&feed=$matches[1]\";s:28:\"projects/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=projects&paged=$matches[1]\";s:11:\"services/?$\";s:28:\"index.php?post_type=services\";s:41:\"services/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=services&feed=$matches[1]\";s:36:\"services/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=services&feed=$matches[1]\";s:28:\"services/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=services&paged=$matches[1]\";s:10:\"careers/?$\";s:27:\"index.php?post_type=careers\";s:40:\"careers/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=careers&feed=$matches[1]\";s:35:\"careers/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=careers&feed=$matches[1]\";s:27:\"careers/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=careers&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"projects/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"projects/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"projects/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"projects/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"projects/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"projects/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"projects/([^/]+)/embed/?$\";s:41:\"index.php?projects=$matches[1]&embed=true\";s:29:\"projects/([^/]+)/trackback/?$\";s:35:\"index.php?projects=$matches[1]&tb=1\";s:49:\"projects/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?projects=$matches[1]&feed=$matches[2]\";s:44:\"projects/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?projects=$matches[1]&feed=$matches[2]\";s:37:\"projects/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?projects=$matches[1]&paged=$matches[2]\";s:44:\"projects/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?projects=$matches[1]&cpage=$matches[2]\";s:33:\"projects/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?projects=$matches[1]&page=$matches[2]\";s:25:\"projects/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"projects/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"projects/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"projects/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"projects/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"projects/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"services/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"services/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"services/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"services/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"services/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"services/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"services/([^/]+)/embed/?$\";s:41:\"index.php?services=$matches[1]&embed=true\";s:29:\"services/([^/]+)/trackback/?$\";s:35:\"index.php?services=$matches[1]&tb=1\";s:49:\"services/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?services=$matches[1]&feed=$matches[2]\";s:44:\"services/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?services=$matches[1]&feed=$matches[2]\";s:37:\"services/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?services=$matches[1]&paged=$matches[2]\";s:44:\"services/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?services=$matches[1]&cpage=$matches[2]\";s:33:\"services/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?services=$matches[1]&page=$matches[2]\";s:25:\"services/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"services/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"services/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"services/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"services/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"services/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:35:\"careers/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"careers/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"careers/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"careers/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"careers/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"careers/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"careers/([^/]+)/embed/?$\";s:40:\"index.php?careers=$matches[1]&embed=true\";s:28:\"careers/([^/]+)/trackback/?$\";s:34:\"index.php?careers=$matches[1]&tb=1\";s:48:\"careers/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?careers=$matches[1]&feed=$matches[2]\";s:43:\"careers/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?careers=$matches[1]&feed=$matches[2]\";s:36:\"careers/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?careers=$matches[1]&paged=$matches[2]\";s:43:\"careers/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?careers=$matches[1]&cpage=$matches[2]\";s:32:\"careers/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?careers=$matches[1]&page=$matches[2]\";s:24:\"careers/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"careers/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"careers/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"careers/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"careers/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"careers/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=14&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:41:\"advanced-custom-fields-pro-master/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:3;s:47:\"really-simple-captcha/really-simple-captcha.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'ebdaa', 'yes'),
(41, 'stylesheet', 'ebdaa', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '14', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'initial_db_version', '38590', 'yes'),
(93, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(94, 'fresh_site', '0', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'cron', 'a:5:{i:1530086998;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1530094198;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1530094234;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1530095326;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(111, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1529503651;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(115, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.6.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.6.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.6-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.6-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.6\";s:7:\"version\";s:5:\"4.9.6\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1530051069;s:15:\"version_checked\";s:5:\"4.9.6\";s:12:\"translations\";a:0:{}}', 'no'),
(120, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1530046356;s:7:\"checked\";a:4:{s:5:\"ebdaa\";s:3:\"1.6\";s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.6\";s:13:\"twentysixteen\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(125, 'can_compress_scripts', '1', 'no'),
(140, 'recently_activated', 'a:0:{}', 'yes'),
(148, 'cptui_new_install', 'false', 'yes'),
(149, 'cptui_post_types', 'a:3:{s:8:\"projects\";a:28:{s:4:\"name\";s:8:\"projects\";s:5:\"label\";s:8:\"Projects\";s:14:\"singular_label\";s:7:\"Project\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:5:\"false\";s:9:\"rest_base\";s:0:\"\";s:11:\"has_archive\";s:4:\"true\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:1:\"5\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:23:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}s:8:\"services\";a:28:{s:4:\"name\";s:8:\"services\";s:5:\"label\";s:8:\"Services\";s:14:\"singular_label\";s:7:\"Service\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:5:\"false\";s:9:\"rest_base\";s:0:\"\";s:11:\"has_archive\";s:4:\"true\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:1:\"6\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:23:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}s:7:\"careers\";a:28:{s:4:\"name\";s:7:\"careers\";s:5:\"label\";s:7:\"Careers\";s:14:\"singular_label\";s:6:\"Career\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:5:\"false\";s:9:\"rest_base\";s:0:\"\";s:11:\"has_archive\";s:4:\"true\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:1:\"7\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:2:{i:0;s:5:\"title\";i:1;s:6:\"editor\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:23:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}}', 'yes'),
(155, 'acf_version', '5.6.10', 'yes'),
(156, 'category_children', 'a:0:{}', 'yes'),
(164, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.0.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1528284608;s:7:\"version\";s:5:\"5.0.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(190, '_site_transient_timeout_browser_143156aedca8214ce63a15cbea76913a', '1530095979', 'no'),
(191, '_site_transient_browser_143156aedca8214ce63a15cbea76913a', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"67.0.3396.87\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(209, 'current_theme', 'Twenty Seventeen/ebdaa', 'yes'),
(210, 'theme_mods_ebdaa', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(211, 'theme_switched', '', 'yes'),
(227, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(301, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1530046255;s:7:\"checked\";a:7:{s:30:\"advanced-custom-fields/acf.php\";s:6:\"4.4.12\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:6:\"5.6.10\";s:19:\"akismet/akismet.php\";s:5:\"4.0.8\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.0.2\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.5.8\";s:9:\"hello.php\";s:3:\"1.7\";s:47:\"really-simple-captcha/really-simple-captcha.php\";s:5:\"2.0.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:6:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"http://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:55:\"http://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.0.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:62:\"http://downloads.wordpress.org/plugin/contact-form-7.5.0.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.5.8\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:67:\"http://downloads.wordpress.org/plugin/custom-post-type-ui.1.5.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:57:\"http://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"really-simple-captcha/really-simple-captcha.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/really-simple-captcha\";s:4:\"slug\";s:21:\"really-simple-captcha\";s:6:\"plugin\";s:47:\"really-simple-captcha/really-simple-captcha.php\";s:11:\"new_version\";s:5:\"2.0.1\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/really-simple-captcha/\";s:7:\"package\";s:69:\"http://downloads.wordpress.org/plugin/really-simple-captcha.2.0.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/really-simple-captcha/assets/icon-256x256.png?rev=1047241\";s:2:\"1x\";s:74:\"https://ps.w.org/really-simple-captcha/assets/icon-128x128.png?rev=1047241\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/really-simple-captcha/assets/banner-1544x500.png?rev=880406\";s:2:\"1x\";s:75:\"https://ps.w.org/really-simple-captcha/assets/banner-772x250.png?rev=880406\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(302, '_transient_timeout_plugin_slugs', '1530074961', 'no'),
(303, '_transient_plugin_slugs', 'a:7:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:41:\"advanced-custom-fields-pro-master/acf.php\";i:2;s:19:\"akismet/akismet.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:5;s:9:\"hello.php\";i:6;s:47:\"really-simple-captcha/really-simple-captcha.php\";}', 'no'),
(305, '_transient_random_seed', 'b4383de62e9e82102c5dd3af90d720f5', 'yes'),
(328, '_site_transient_timeout_theme_roots', '1530052875', 'no'),
(329, '_site_transient_theme_roots', 'a:4:{s:5:\"ebdaa\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(341, '_transient_is_multi_author', '0', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 6, '_edit_last', '1'),
(4, 6, 'field_5b17b89f30bce', 'a:15:{s:3:\"key\";s:19:\"field_5b17b89f30bce\";s:5:\"label\";s:8:\"Duration\";s:4:\"name\";s:8:\"duration\";s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"Duration\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(6, 6, 'position', 'acf_after_title'),
(7, 6, 'layout', 'no_box'),
(8, 6, 'hide_on_screen', ''),
(9, 6, '_edit_lock', '1528284641:1'),
(10, 6, 'field_5b17b92713aa8', 'a:11:{s:3:\"key\";s:19:\"field_5b17b92713aa8\";s:5:\"label\";s:7:\"Gallery\";s:4:\"name\";s:7:\"gallery\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(12, 14, '_edit_last', '1'),
(13, 14, '_edit_lock', '1529937003:1'),
(14, 16, '_edit_last', '1'),
(15, 16, 'field_5b17be28571af', 'a:11:{s:3:\"key\";s:19:\"field_5b17be28571af\";s:5:\"label\";s:6:\"Slider\";s:4:\"name\";s:6:\"slider\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(17, 16, 'position', 'normal'),
(18, 16, 'layout', 'no_box'),
(19, 16, 'hide_on_screen', ''),
(20, 16, '_edit_lock', '1528282833:1'),
(21, 17, '_edit_last', '1'),
(22, 17, '_edit_lock', '1529989411:1'),
(23, 19, '_edit_last', '1'),
(24, 19, 'field_5b17beede67a8', 'a:11:{s:3:\"key\";s:19:\"field_5b17beede67a8\";s:5:\"label\";s:11:\"Our Mission\";s:4:\"name\";s:11:\"our_mission\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(26, 19, 'position', 'normal'),
(27, 19, 'layout', 'no_box'),
(28, 19, 'hide_on_screen', 'a:1:{i:0;s:11:\"the_content\";}'),
(29, 19, '_edit_lock', '1528282972:1'),
(31, 16, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"14\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(32, 19, 'field_5b17bf90af0be', 'a:11:{s:3:\"key\";s:19:\"field_5b17bf90af0be\";s:5:\"label\";s:10:\"Our Vision\";s:4:\"name\";s:10:\"our_vision\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(34, 19, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"17\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(35, 20, '_edit_last', '1'),
(36, 20, '_edit_lock', '1529990516:1'),
(37, 22, '_edit_last', '1'),
(38, 22, '_edit_lock', '1528283558:1'),
(39, 23, '_edit_last', '1'),
(40, 23, 'field_5b17c0f7449eb', 'a:11:{s:3:\"key\";s:19:\"field_5b17c0f7449eb\";s:5:\"label\";s:8:\"location\";s:4:\"name\";s:8:\"location\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(42, 23, 'position', 'normal'),
(43, 23, 'layout', 'no_box'),
(44, 23, 'hide_on_screen', 'a:1:{i:0;s:11:\"the_content\";}'),
(45, 23, '_edit_lock', '1528284327:1'),
(47, 23, 'field_5b17c14514635', 'a:12:{s:3:\"key\";s:19:\"field_5b17c14514635\";s:5:\"label\";s:3:\"Map\";s:4:\"name\";s:3:\"map\";s:4:\"type\";s:10:\"google_map\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"center_lat\";s:0:\"\";s:10:\"center_lng\";s:0:\"\";s:4:\"zoom\";s:0:\"\";s:6:\"height\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(50, 22, '_wp_trash_meta_status', 'draft'),
(51, 22, '_wp_trash_meta_time', '1528283729'),
(52, 22, '_wp_desired_post_slug', ''),
(53, 6, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"projects\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(56, 23, 'field_5b17c4b31989c', 'a:14:{s:3:\"key\";s:19:\"field_5b17c4b31989c\";s:5:\"label\";s:12:\"BannerTittle\";s:4:\"name\";s:12:\"bannertittle\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(58, 23, 'field_5b17c4ff0ada9', 'a:14:{s:3:\"key\";s:19:\"field_5b17c4ff0ada9\";s:5:\"label\";s:15:\"BannerSubTittle\";s:4:\"name\";s:15:\"bannersubtittle\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(59, 23, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"20\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(60, 25, '_form', '<div class=\"form-group\"><div class=\"col-sm-12\">\n[text* your-name class:form-control placeholder \"Name\"]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[email* your-email class:form-control placeholder \"Email\"]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[text your-subject class:form-control placeholder \"subject\"]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[textarea your-message class:form-control placeholder \"Your Message\"]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\nInput this code: [captchac captcha-778 size:l fg:#ffffff bg:#000000][captchar captcha-778 4/4]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[submit \"Submit Message\"]</div></div>'),
(61, 25, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:22:\"EBDAA \"[your-subject]\"\";s:6:\"sender\";s:34:\"[your-name] <m.rohouma1@gmail.com>\";s:9:\"recipient\";s:20:\"m.rohouma1@gmail.com\";s:4:\"body\";s:166:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(62, 25, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:22:\"EBDAA \"[your-subject]\"\";s:6:\"sender\";s:28:\"EBDAA <m.rohouma1@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:108:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\";s:18:\"additional_headers\";s:30:\"Reply-To: m.rohouma1@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(63, 25, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(64, 25, '_additional_settings', ''),
(65, 25, '_locale', 'en_US'),
(66, 34, '_edit_lock', '1530059258:1'),
(67, 34, '_edit_last', '1'),
(68, 3, '_wp_trash_meta_status', 'draft'),
(69, 3, '_wp_trash_meta_time', '1529416261'),
(70, 3, '_wp_desired_post_slug', 'privacy-policy'),
(71, 29, '_edit_lock', '1529932605:1'),
(72, 29, '_edit_last', '1'),
(73, 50, '_menu_item_type', 'post_type'),
(74, 50, '_menu_item_menu_item_parent', '0'),
(75, 50, '_menu_item_object_id', '20'),
(76, 50, '_menu_item_object', 'page'),
(77, 50, '_menu_item_target', ''),
(78, 50, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(79, 50, '_menu_item_xfn', ''),
(80, 50, '_menu_item_url', ''),
(82, 51, '_menu_item_type', 'post_type'),
(83, 51, '_menu_item_menu_item_parent', '0'),
(84, 51, '_menu_item_object_id', '17'),
(85, 51, '_menu_item_object', 'page'),
(86, 51, '_menu_item_target', ''),
(87, 51, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(88, 51, '_menu_item_xfn', ''),
(89, 51, '_menu_item_url', ''),
(91, 52, '_menu_item_type', 'post_type'),
(92, 52, '_menu_item_menu_item_parent', '0'),
(93, 52, '_menu_item_object_id', '14'),
(94, 52, '_menu_item_object', 'page'),
(95, 52, '_menu_item_target', ''),
(96, 52, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(97, 52, '_menu_item_xfn', ''),
(98, 52, '_menu_item_url', ''),
(100, 53, '_edit_last', '1'),
(101, 53, '_edit_lock', '1530059195:1'),
(102, 53, 'duration', ''),
(103, 53, '_duration', 'field_5b17b89f30bce'),
(104, 53, 'gallery', 'a:8:{i:0;s:3:\"137\";i:1;s:3:\"138\";i:2;s:3:\"139\";i:3;s:3:\"140\";i:4;s:3:\"141\";i:5;s:3:\"142\";i:6;s:3:\"143\";i:7;s:3:\"144\";}'),
(105, 53, '_gallery', 'field_5b17b92713aa8'),
(106, 54, '_edit_last', '1'),
(107, 54, '_edit_lock', '1530057740:1'),
(108, 55, '_edit_last', '1'),
(109, 55, '_edit_lock', '1530061427:1'),
(110, 56, '_menu_item_type', 'post_type'),
(111, 56, '_menu_item_menu_item_parent', '0'),
(112, 56, '_menu_item_object_id', '53'),
(113, 56, '_menu_item_object', 'projects'),
(114, 56, '_menu_item_target', ''),
(115, 56, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(116, 56, '_menu_item_xfn', ''),
(117, 56, '_menu_item_url', ''),
(118, 56, '_menu_item_orphaned', '1529585204'),
(119, 57, '_edit_last', '1'),
(120, 57, 'duration', ''),
(121, 57, '_duration', 'field_5b17b89f30bce'),
(122, 57, 'gallery', ''),
(123, 57, '_gallery', 'field_5b17b92713aa8'),
(124, 57, '_edit_lock', '1530055450:1'),
(125, 58, '_edit_last', '1'),
(126, 58, '_edit_lock', '1529990946:1'),
(127, 59, '_edit_last', '1'),
(128, 59, '_edit_lock', '1529585258:1'),
(129, 60, '_menu_item_type', 'post_type'),
(130, 60, '_menu_item_menu_item_parent', '0'),
(131, 60, '_menu_item_object_id', '53'),
(132, 60, '_menu_item_object', 'projects'),
(133, 60, '_menu_item_target', ''),
(134, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(135, 60, '_menu_item_xfn', ''),
(136, 60, '_menu_item_url', ''),
(137, 60, '_menu_item_orphaned', '1529585413'),
(138, 61, '_menu_item_type', 'post_type'),
(139, 61, '_menu_item_menu_item_parent', '0'),
(140, 61, '_menu_item_object_id', '57'),
(141, 61, '_menu_item_object', 'projects'),
(142, 61, '_menu_item_target', ''),
(143, 61, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(144, 61, '_menu_item_xfn', ''),
(145, 61, '_menu_item_url', ''),
(146, 61, '_menu_item_orphaned', '1529585414'),
(147, 62, '_menu_item_type', 'post_type'),
(148, 62, '_menu_item_menu_item_parent', '0'),
(149, 62, '_menu_item_object_id', '58'),
(150, 62, '_menu_item_object', 'services'),
(151, 62, '_menu_item_target', ''),
(152, 62, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(153, 62, '_menu_item_xfn', ''),
(154, 62, '_menu_item_url', ''),
(155, 62, '_menu_item_orphaned', '1529585471'),
(156, 63, '_menu_item_type', 'post_type'),
(157, 63, '_menu_item_menu_item_parent', '0'),
(158, 63, '_menu_item_object_id', '54'),
(159, 63, '_menu_item_object', 'services'),
(160, 63, '_menu_item_target', ''),
(161, 63, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(162, 63, '_menu_item_xfn', ''),
(163, 63, '_menu_item_url', ''),
(164, 63, '_menu_item_orphaned', '1529585471'),
(165, 20, '_wp_page_template', 'page-contact.php'),
(166, 20, 'telephone', '(+2)01145978781'),
(167, 20, '_telephone', 'field_5b2b9be31f554'),
(168, 20, 'email', 'info@ebdaa-eng.com'),
(169, 20, '_email', 'field_5b2b9c051f555'),
(170, 20, 'location', '286 Emtidad Rasmsis 2,\r\nNaser City - Cairo - Egypt'),
(171, 20, '_location', 'field_5b17c0f7449eb'),
(172, 20, 'map', ''),
(173, 20, '_map', 'field_5b17c14514635'),
(174, 20, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(175, 20, '_facebook', 'field_5b2b9c341f556'),
(176, 20, 'twitter', ''),
(177, 20, '_twitter', 'field_5b2b9c4c1f557'),
(178, 20, 'linked_in', ''),
(179, 20, '_linked_in', 'field_5b2b9c721f558'),
(180, 20, 'google', ''),
(181, 20, '_google', 'field_5b2b9c981f559'),
(182, 20, 'youtube', ''),
(183, 20, '_youtube', 'field_5b2b9cd31f55a'),
(184, 21, 'telephone', ''),
(185, 21, '_telephone', 'field_5b2b9be31f554'),
(186, 21, 'email', ''),
(187, 21, '_email', 'field_5b2b9c051f555'),
(188, 21, 'location', ''),
(189, 21, '_location', 'field_5b17c0f7449eb'),
(190, 21, 'map', ''),
(191, 21, '_map', 'field_5b17c14514635'),
(192, 21, 'facebook', ''),
(193, 21, '_facebook', 'field_5b2b9c341f556'),
(194, 21, 'twitter', ''),
(195, 21, '_twitter', 'field_5b2b9c4c1f557'),
(196, 21, 'linked_in', ''),
(197, 21, '_linked_in', 'field_5b2b9c721f558'),
(198, 21, 'google', ''),
(199, 21, '_google', 'field_5b2b9c981f559'),
(200, 21, 'youtube', ''),
(201, 21, '_youtube', 'field_5b2b9cd31f55a'),
(202, 64, 'telephone', ''),
(203, 64, '_telephone', 'field_5b2b9be31f554'),
(204, 64, 'email', ''),
(205, 64, '_email', 'field_5b2b9c051f555'),
(206, 64, 'location', ''),
(207, 64, '_location', 'field_5b17c0f7449eb'),
(208, 64, 'map', ''),
(209, 64, '_map', 'field_5b17c14514635'),
(210, 64, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(211, 64, '_facebook', 'field_5b2b9c341f556'),
(212, 64, 'twitter', ''),
(213, 64, '_twitter', 'field_5b2b9c4c1f557'),
(214, 64, 'linked_in', ''),
(215, 64, '_linked_in', 'field_5b2b9c721f558'),
(216, 64, 'google', ''),
(217, 64, '_google', 'field_5b2b9c981f559'),
(218, 64, 'youtube', ''),
(219, 64, '_youtube', 'field_5b2b9cd31f55a'),
(220, 65, 'telephone', '(+2)01145978781'),
(221, 65, '_telephone', 'field_5b2b9be31f554'),
(222, 65, 'email', 'info@ebdaa-eng.com'),
(223, 65, '_email', 'field_5b2b9c051f555'),
(224, 65, 'location', ''),
(225, 65, '_location', 'field_5b17c0f7449eb'),
(226, 65, 'map', ''),
(227, 65, '_map', 'field_5b17c14514635'),
(228, 65, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(229, 65, '_facebook', 'field_5b2b9c341f556'),
(230, 65, 'twitter', ''),
(231, 65, '_twitter', 'field_5b2b9c4c1f557'),
(232, 65, 'linked_in', ''),
(233, 65, '_linked_in', 'field_5b2b9c721f558'),
(234, 65, 'google', ''),
(235, 65, '_google', 'field_5b2b9c981f559'),
(236, 65, 'youtube', ''),
(237, 65, '_youtube', 'field_5b2b9cd31f55a'),
(238, 66, 'telephone', '(+2)01145978781'),
(239, 66, '_telephone', 'field_5b2b9be31f554'),
(240, 66, 'email', ''),
(241, 66, '_email', 'field_5b2b9c051f555'),
(242, 66, 'location', ''),
(243, 66, '_location', 'field_5b17c0f7449eb'),
(244, 66, 'map', ''),
(245, 66, '_map', 'field_5b17c14514635'),
(246, 66, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(247, 66, '_facebook', 'field_5b2b9c341f556'),
(248, 66, 'twitter', ''),
(249, 66, '_twitter', 'field_5b2b9c4c1f557'),
(250, 66, 'linked_in', ''),
(251, 66, '_linked_in', 'field_5b2b9c721f558'),
(252, 66, 'google', ''),
(253, 66, '_google', 'field_5b2b9c981f559'),
(254, 66, 'youtube', ''),
(255, 66, '_youtube', 'field_5b2b9cd31f55a'),
(256, 67, 'telephone', '(+2)01145978781'),
(257, 67, '_telephone', 'field_5b2b9be31f554'),
(258, 67, 'email', 'info@ebdaa-eng.com'),
(259, 67, '_email', 'field_5b2b9c051f555'),
(260, 67, 'location', ''),
(261, 67, '_location', 'field_5b17c0f7449eb'),
(262, 67, 'map', ''),
(263, 67, '_map', 'field_5b17c14514635'),
(264, 67, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(265, 67, '_facebook', 'field_5b2b9c341f556'),
(266, 67, 'twitter', ''),
(267, 67, '_twitter', 'field_5b2b9c4c1f557'),
(268, 67, 'linked_in', ''),
(269, 67, '_linked_in', 'field_5b2b9c721f558'),
(270, 67, 'google', ''),
(271, 67, '_google', 'field_5b2b9c981f559'),
(272, 67, 'youtube', ''),
(273, 67, '_youtube', 'field_5b2b9cd31f55a'),
(274, 68, '_menu_item_type', 'custom'),
(275, 68, '_menu_item_menu_item_parent', '0'),
(276, 68, '_menu_item_object_id', '68'),
(277, 68, '_menu_item_object', 'custom'),
(278, 68, '_menu_item_target', ''),
(279, 68, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(280, 68, '_menu_item_xfn', ''),
(281, 68, '_menu_item_url', '#'),
(282, 68, '_menu_item_orphaned', '1529925930'),
(283, 69, '_menu_item_type', 'post_type'),
(284, 69, '_menu_item_menu_item_parent', '0'),
(285, 69, '_menu_item_object_id', '55'),
(286, 69, '_menu_item_object', 'careers'),
(287, 69, '_menu_item_target', ''),
(288, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(289, 69, '_menu_item_xfn', ''),
(290, 69, '_menu_item_url', ''),
(291, 69, '_menu_item_orphaned', '1529925947'),
(292, 70, '_menu_item_type', 'post_type'),
(293, 70, '_menu_item_menu_item_parent', '0'),
(294, 70, '_menu_item_object_id', '59'),
(295, 70, '_menu_item_object', 'careers'),
(296, 70, '_menu_item_target', ''),
(297, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(298, 70, '_menu_item_xfn', ''),
(299, 70, '_menu_item_url', ''),
(300, 70, '_menu_item_orphaned', '1529925948'),
(301, 71, '_edit_last', '1'),
(302, 71, 'duration', ''),
(303, 71, '_duration', 'field_5b17b89f30bce'),
(304, 71, 'gallery', ''),
(305, 71, '_gallery', 'field_5b17b92713aa8'),
(306, 71, '_edit_lock', '1529925850:1'),
(307, 72, '_edit_last', '1'),
(308, 72, 'duration', ''),
(309, 72, '_duration', 'field_5b17b89f30bce'),
(310, 72, 'gallery', ''),
(311, 72, '_gallery', 'field_5b17b92713aa8'),
(312, 72, '_edit_lock', '1529925858:1'),
(313, 73, '_edit_last', '1'),
(314, 73, 'duration', ''),
(315, 73, '_duration', 'field_5b17b89f30bce'),
(316, 73, 'gallery', ''),
(317, 73, '_gallery', 'field_5b17b92713aa8'),
(318, 73, '_edit_lock', '1529925968:1'),
(346, 77, '_menu_item_type', 'post_type'),
(347, 77, '_menu_item_menu_item_parent', '0'),
(348, 77, '_menu_item_object_id', '71'),
(349, 77, '_menu_item_object', 'projects'),
(350, 77, '_menu_item_target', ''),
(351, 77, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(352, 77, '_menu_item_xfn', ''),
(353, 77, '_menu_item_url', ''),
(354, 77, '_menu_item_orphaned', '1529926065'),
(355, 78, '_menu_item_type', 'post_type'),
(356, 78, '_menu_item_menu_item_parent', '0'),
(357, 78, '_menu_item_object_id', '72'),
(358, 78, '_menu_item_object', 'projects'),
(359, 78, '_menu_item_target', ''),
(360, 78, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(361, 78, '_menu_item_xfn', ''),
(362, 78, '_menu_item_url', ''),
(363, 78, '_menu_item_orphaned', '1529926066'),
(364, 79, '_menu_item_type', 'post_type'),
(365, 79, '_menu_item_menu_item_parent', '0'),
(366, 79, '_menu_item_object_id', '73'),
(367, 79, '_menu_item_object', 'projects'),
(368, 79, '_menu_item_target', ''),
(369, 79, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(370, 79, '_menu_item_xfn', ''),
(371, 79, '_menu_item_url', ''),
(372, 79, '_menu_item_orphaned', '1529926067'),
(373, 80, '_menu_item_type', 'post_type'),
(374, 80, '_menu_item_menu_item_parent', '0'),
(375, 80, '_menu_item_object_id', '53'),
(376, 80, '_menu_item_object', 'projects'),
(377, 80, '_menu_item_target', ''),
(378, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(379, 80, '_menu_item_xfn', ''),
(380, 80, '_menu_item_url', ''),
(381, 80, '_menu_item_orphaned', '1529926067'),
(382, 81, '_menu_item_type', 'post_type'),
(383, 81, '_menu_item_menu_item_parent', '0'),
(384, 81, '_menu_item_object_id', '57'),
(385, 81, '_menu_item_object', 'projects'),
(386, 81, '_menu_item_target', ''),
(387, 81, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(388, 81, '_menu_item_xfn', ''),
(389, 81, '_menu_item_url', ''),
(390, 81, '_menu_item_orphaned', '1529926068'),
(391, 82, '_edit_last', '1'),
(392, 82, 'duration', ''),
(393, 82, '_duration', 'field_5b17b89f30bce'),
(394, 82, 'gallery', ''),
(395, 82, '_gallery', 'field_5b17b92713aa8'),
(396, 82, '_edit_lock', '1529928849:1'),
(397, 83, '_menu_item_type', 'post_type'),
(398, 83, '_menu_item_menu_item_parent', '0'),
(399, 83, '_menu_item_object_id', '82'),
(400, 83, '_menu_item_object', 'projects'),
(401, 83, '_menu_item_target', ''),
(402, 83, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(403, 83, '_menu_item_xfn', ''),
(404, 83, '_menu_item_url', ''),
(405, 83, '_menu_item_orphaned', '1529926134'),
(406, 84, '_menu_item_type', 'post_type'),
(407, 84, '_menu_item_menu_item_parent', '0'),
(408, 84, '_menu_item_object_id', '73'),
(409, 84, '_menu_item_object', 'projects'),
(410, 84, '_menu_item_target', ''),
(411, 84, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(412, 84, '_menu_item_xfn', ''),
(413, 84, '_menu_item_url', ''),
(414, 84, '_menu_item_orphaned', '1529926134'),
(415, 85, '_menu_item_type', 'post_type'),
(416, 85, '_menu_item_menu_item_parent', '0'),
(417, 85, '_menu_item_object_id', '72'),
(418, 85, '_menu_item_object', 'projects'),
(419, 85, '_menu_item_target', ''),
(420, 85, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(421, 85, '_menu_item_xfn', ''),
(422, 85, '_menu_item_url', ''),
(423, 85, '_menu_item_orphaned', '1529926135'),
(424, 86, '_menu_item_type', 'post_type'),
(425, 86, '_menu_item_menu_item_parent', '0'),
(426, 86, '_menu_item_object_id', '71'),
(427, 86, '_menu_item_object', 'projects'),
(428, 86, '_menu_item_target', ''),
(429, 86, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(430, 86, '_menu_item_xfn', ''),
(431, 86, '_menu_item_url', ''),
(432, 86, '_menu_item_orphaned', '1529926136'),
(433, 87, '_menu_item_type', 'post_type'),
(434, 87, '_menu_item_menu_item_parent', '0'),
(435, 87, '_menu_item_object_id', '57'),
(436, 87, '_menu_item_object', 'projects'),
(437, 87, '_menu_item_target', ''),
(438, 87, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(439, 87, '_menu_item_xfn', ''),
(440, 87, '_menu_item_url', ''),
(441, 87, '_menu_item_orphaned', '1529926136'),
(442, 88, '_menu_item_type', 'post_type'),
(443, 88, '_menu_item_menu_item_parent', '0'),
(444, 88, '_menu_item_object_id', '53'),
(445, 88, '_menu_item_object', 'projects'),
(446, 88, '_menu_item_target', ''),
(447, 88, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(448, 88, '_menu_item_xfn', ''),
(449, 88, '_menu_item_url', ''),
(450, 88, '_menu_item_orphaned', '1529926137'),
(469, 2, '_wp_trash_meta_status', 'publish'),
(470, 2, '_wp_trash_meta_time', '1529926430'),
(471, 2, '_wp_desired_post_slug', 'sample-page'),
(472, 92, '_menu_item_type', 'post_type'),
(473, 92, '_menu_item_menu_item_parent', '123'),
(474, 92, '_menu_item_object_id', '58'),
(475, 92, '_menu_item_object', 'services'),
(476, 92, '_menu_item_target', ''),
(477, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(478, 92, '_menu_item_xfn', ''),
(479, 92, '_menu_item_url', ''),
(481, 93, '_menu_item_type', 'post_type'),
(482, 93, '_menu_item_menu_item_parent', '123'),
(483, 93, '_menu_item_object_id', '54'),
(484, 93, '_menu_item_object', 'services'),
(485, 93, '_menu_item_target', ''),
(486, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(487, 93, '_menu_item_xfn', ''),
(488, 93, '_menu_item_url', ''),
(490, 95, '_wp_attached_file', '2018/06/pro.pdf'),
(491, 20, 'brochure', '95'),
(492, 20, '_brochure', 'field_5b30dca674b12'),
(493, 96, 'telephone', '(+2)01145978781'),
(494, 96, '_telephone', 'field_5b2b9be31f554'),
(495, 96, 'email', 'info@ebdaa-eng.com'),
(496, 96, '_email', 'field_5b2b9c051f555'),
(497, 96, 'location', ''),
(498, 96, '_location', 'field_5b17c0f7449eb'),
(499, 96, 'map', ''),
(500, 96, '_map', 'field_5b17c14514635'),
(501, 96, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(502, 96, '_facebook', 'field_5b2b9c341f556'),
(503, 96, 'twitter', ''),
(504, 96, '_twitter', 'field_5b2b9c4c1f557'),
(505, 96, 'linked_in', ''),
(506, 96, '_linked_in', 'field_5b2b9c721f558'),
(507, 96, 'google', ''),
(508, 96, '_google', 'field_5b2b9c981f559'),
(509, 96, 'youtube', ''),
(510, 96, '_youtube', 'field_5b2b9cd31f55a'),
(511, 96, 'brochure', '95'),
(512, 96, '_brochure', 'field_5b30dca674b12'),
(513, 20, 'telephone2', '(+202)20809095'),
(514, 20, '_telephone2', 'field_5b30e1e9c674c'),
(515, 98, 'telephone', '(+2)01145978781'),
(516, 98, '_telephone', 'field_5b2b9be31f554'),
(517, 98, 'email', 'info@ebdaa-eng.com'),
(518, 98, '_email', 'field_5b2b9c051f555'),
(519, 98, 'location', ''),
(520, 98, '_location', 'field_5b17c0f7449eb'),
(521, 98, 'map', ''),
(522, 98, '_map', 'field_5b17c14514635'),
(523, 98, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(524, 98, '_facebook', 'field_5b2b9c341f556'),
(525, 98, 'twitter', ''),
(526, 98, '_twitter', 'field_5b2b9c4c1f557'),
(527, 98, 'linked_in', ''),
(528, 98, '_linked_in', 'field_5b2b9c721f558'),
(529, 98, 'google', ''),
(530, 98, '_google', 'field_5b2b9c981f559'),
(531, 98, 'youtube', ''),
(532, 98, '_youtube', 'field_5b2b9cd31f55a'),
(533, 98, 'brochure', '95'),
(534, 98, '_brochure', 'field_5b30dca674b12'),
(535, 98, 'telephone2', '(+202)20809095'),
(536, 98, '_telephone2', 'field_5b30e1e9c674c'),
(537, 99, 'telephone', '(+2)01145978781'),
(538, 99, '_telephone', 'field_5b2b9be31f554'),
(539, 99, 'email', 'info@ebdaa-eng.com'),
(540, 99, '_email', 'field_5b2b9c051f555'),
(541, 99, 'location', '286 Emtidad Rasmsis 2,\r\nNaser City - Cairo - Egypt'),
(542, 99, '_location', 'field_5b17c0f7449eb'),
(543, 99, 'map', ''),
(544, 99, '_map', 'field_5b17c14514635'),
(545, 99, 'facebook', 'https://www.facebook.com/Ebdaa-Engineering-company-533896603440835/'),
(546, 99, '_facebook', 'field_5b2b9c341f556'),
(547, 99, 'twitter', ''),
(548, 99, '_twitter', 'field_5b2b9c4c1f557'),
(549, 99, 'linked_in', ''),
(550, 99, '_linked_in', 'field_5b2b9c721f558'),
(551, 99, 'google', ''),
(552, 99, '_google', 'field_5b2b9c981f559'),
(553, 99, 'youtube', ''),
(554, 99, '_youtube', 'field_5b2b9cd31f55a'),
(555, 99, 'brochure', '95'),
(556, 99, '_brochure', 'field_5b30dca674b12'),
(557, 99, 'telephone2', '(+202)20809095'),
(558, 99, '_telephone2', 'field_5b30e1e9c674c'),
(559, 37, '_edit_lock', '1529974532:1'),
(560, 37, '_edit_last', '1'),
(561, 105, '_wp_attached_file', '2018/06/slider1.jpg'),
(562, 105, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:606;s:4:\"file\";s:19:\"2018/06/slider1.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"slider1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"slider1-300x114.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:114;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"slider1-768x291.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:291;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"slider1-1024x388.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:388;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:19:\"slider1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(563, 14, '_wp_page_template', 'default'),
(564, 14, 'slider_0_image', '105'),
(565, 14, '_slider_0_image', 'field_5b30ebf34753d'),
(566, 14, 'slider_0_title', 'Welcome To Our Company'),
(567, 14, '_slider_0_title', 'field_5b30ec184753e'),
(568, 14, 'slider_0_sub_title', 'We Provide Fast And Reliable Service to Clients'),
(569, 14, '_slider_0_sub_title', 'field_5b30ec2d4753f'),
(570, 14, 'slider_0_link', 'http://localhost/ebdaa/about-us/'),
(571, 14, '_slider_0_link', 'field_5b30ec4347540'),
(572, 14, 'slider_0_link_text', 'About us'),
(573, 14, '_slider_0_link_text', 'field_5b30ec5747541'),
(574, 14, 'slider', '2'),
(575, 14, '_slider', 'field_5b17be28571af'),
(576, 106, 'slider_0_image', '105'),
(577, 106, '_slider_0_image', 'field_5b30ebf34753d'),
(578, 106, 'slider_0_title', 'Welcome To Our Company'),
(579, 106, '_slider_0_title', 'field_5b30ec184753e'),
(580, 106, 'slider_0_sub_title', 'We Provide Fast And Reliable Service to Clients'),
(581, 106, '_slider_0_sub_title', 'field_5b30ec2d4753f'),
(582, 106, 'slider_0_link', ''),
(583, 106, '_slider_0_link', 'field_5b30ec4347540'),
(584, 106, 'slider_0_link_text', 'About us'),
(585, 106, '_slider_0_link_text', 'field_5b30ec5747541'),
(586, 106, 'slider', '1'),
(587, 106, '_slider', 'field_5b17be28571af'),
(588, 107, '_wp_attached_file', '2018/06/slider2.jpg'),
(589, 107, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:606;s:4:\"file\";s:19:\"2018/06/slider2.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"slider2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"slider2-300x114.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:114;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"slider2-768x291.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:291;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"slider2-1024x388.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:388;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:19:\"slider2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(590, 14, 'slider_1_image', '107'),
(591, 14, '_slider_1_image', 'field_5b30ebf34753d'),
(592, 14, 'slider_1_title', 'Welcome To Our Company 2'),
(593, 14, '_slider_1_title', 'field_5b30ec184753e'),
(594, 14, 'slider_1_sub_title', 'We Provide Fast And Reliable Service to Clients 2'),
(595, 14, '_slider_1_sub_title', 'field_5b30ec2d4753f'),
(596, 14, 'slider_1_link_text', 'Service'),
(597, 14, '_slider_1_link_text', 'field_5b30ec5747541'),
(598, 14, 'slider_1_link', ''),
(599, 14, '_slider_1_link', 'field_5b30ec4347540'),
(600, 108, 'slider_0_image', '105'),
(601, 108, '_slider_0_image', 'field_5b30ebf34753d'),
(602, 108, 'slider_0_title', 'Welcome To Our Company'),
(603, 108, '_slider_0_title', 'field_5b30ec184753e'),
(604, 108, 'slider_0_sub_title', 'We Provide Fast And Reliable Service to Clients'),
(605, 108, '_slider_0_sub_title', 'field_5b30ec2d4753f'),
(606, 108, 'slider_0_link', ''),
(607, 108, '_slider_0_link', 'field_5b30ec4347540'),
(608, 108, 'slider_0_link_text', 'About us'),
(609, 108, '_slider_0_link_text', 'field_5b30ec5747541'),
(610, 108, 'slider', '2'),
(611, 108, '_slider', 'field_5b17be28571af'),
(612, 108, 'slider_1_image', '107'),
(613, 108, '_slider_1_image', 'field_5b30ebf34753d'),
(614, 108, 'slider_1_title', 'Welcome To Our Company 2'),
(615, 108, '_slider_1_title', 'field_5b30ec184753e'),
(616, 108, 'slider_1_sub_title', 'We Provide Fast And Reliable Service to Clients 2'),
(617, 108, '_slider_1_sub_title', 'field_5b30ec2d4753f'),
(618, 108, 'slider_1_link_text', 'Service'),
(619, 108, '_slider_1_link_text', 'field_5b30ec5747541'),
(620, 108, 'slider_1_link', ''),
(621, 108, '_slider_1_link', 'field_5b30ec4347540'),
(622, 109, 'slider_0_image', '105'),
(623, 109, '_slider_0_image', 'field_5b30ebf34753d'),
(624, 109, 'slider_0_title', 'Welcome To Our Company'),
(625, 109, '_slider_0_title', 'field_5b30ec184753e'),
(626, 109, 'slider_0_sub_title', 'We Provide Fast And Reliable Service to Clients'),
(627, 109, '_slider_0_sub_title', 'field_5b30ec2d4753f'),
(628, 109, 'slider_0_link', 'http://localhost/ebdaa/about-us/'),
(629, 109, '_slider_0_link', 'field_5b30ec4347540'),
(630, 109, 'slider_0_link_text', 'About us'),
(631, 109, '_slider_0_link_text', 'field_5b30ec5747541'),
(632, 109, 'slider', '2'),
(633, 109, '_slider', 'field_5b17be28571af'),
(634, 109, 'slider_1_image', '107'),
(635, 109, '_slider_1_image', 'field_5b30ebf34753d'),
(636, 109, 'slider_1_title', 'Welcome To Our Company 2'),
(637, 109, '_slider_1_title', 'field_5b30ec184753e'),
(638, 109, 'slider_1_sub_title', 'We Provide Fast And Reliable Service to Clients 2'),
(639, 109, '_slider_1_sub_title', 'field_5b30ec2d4753f'),
(640, 109, 'slider_1_link_text', 'Service'),
(641, 109, '_slider_1_link_text', 'field_5b30ec5747541'),
(642, 109, 'slider_1_link', ''),
(643, 109, '_slider_1_link', 'field_5b30ec4347540'),
(644, 110, '_menu_item_type', 'post_type'),
(645, 110, '_menu_item_menu_item_parent', '0'),
(646, 110, '_menu_item_object_id', '82'),
(647, 110, '_menu_item_object', 'projects'),
(648, 110, '_menu_item_target', ''),
(649, 110, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(650, 110, '_menu_item_xfn', ''),
(651, 110, '_menu_item_url', ''),
(652, 110, '_menu_item_orphaned', '1529974691'),
(653, 111, '_menu_item_type', 'post_type'),
(654, 111, '_menu_item_menu_item_parent', '0'),
(655, 111, '_menu_item_object_id', '73'),
(656, 111, '_menu_item_object', 'projects'),
(657, 111, '_menu_item_target', ''),
(658, 111, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(659, 111, '_menu_item_xfn', ''),
(660, 111, '_menu_item_url', ''),
(661, 111, '_menu_item_orphaned', '1529974692'),
(662, 112, '_menu_item_type', 'post_type'),
(663, 112, '_menu_item_menu_item_parent', '0'),
(664, 112, '_menu_item_object_id', '72'),
(665, 112, '_menu_item_object', 'projects'),
(666, 112, '_menu_item_target', ''),
(667, 112, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(668, 112, '_menu_item_xfn', ''),
(669, 112, '_menu_item_url', ''),
(670, 112, '_menu_item_orphaned', '1529974692'),
(671, 113, '_menu_item_type', 'post_type'),
(672, 113, '_menu_item_menu_item_parent', '0'),
(673, 113, '_menu_item_object_id', '71'),
(674, 113, '_menu_item_object', 'projects'),
(675, 113, '_menu_item_target', ''),
(676, 113, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(677, 113, '_menu_item_xfn', ''),
(678, 113, '_menu_item_url', ''),
(679, 113, '_menu_item_orphaned', '1529974693'),
(680, 114, '_menu_item_type', 'post_type'),
(681, 114, '_menu_item_menu_item_parent', '0'),
(682, 114, '_menu_item_object_id', '57'),
(683, 114, '_menu_item_object', 'projects'),
(684, 114, '_menu_item_target', ''),
(685, 114, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(686, 114, '_menu_item_xfn', ''),
(687, 114, '_menu_item_url', ''),
(688, 114, '_menu_item_orphaned', '1529974693'),
(689, 115, '_menu_item_type', 'post_type'),
(690, 115, '_menu_item_menu_item_parent', '0'),
(691, 115, '_menu_item_object_id', '53'),
(692, 115, '_menu_item_object', 'projects'),
(693, 115, '_menu_item_target', ''),
(694, 115, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(695, 115, '_menu_item_xfn', ''),
(696, 115, '_menu_item_url', ''),
(697, 115, '_menu_item_orphaned', '1529974694'),
(698, 116, '_menu_item_type', 'post_type_archive'),
(699, 116, '_menu_item_menu_item_parent', '0'),
(700, 116, '_menu_item_object_id', '-16'),
(701, 116, '_menu_item_object', 'projects'),
(702, 116, '_menu_item_target', ''),
(703, 116, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(704, 116, '_menu_item_xfn', ''),
(705, 116, '_menu_item_url', ''),
(706, 116, '_menu_item_orphaned', '1529974713'),
(707, 117, '_menu_item_type', 'post_type'),
(708, 117, '_menu_item_menu_item_parent', '0'),
(709, 117, '_menu_item_object_id', '71'),
(710, 117, '_menu_item_object', 'projects'),
(711, 117, '_menu_item_target', ''),
(712, 117, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(713, 117, '_menu_item_xfn', ''),
(714, 117, '_menu_item_url', ''),
(715, 117, '_menu_item_orphaned', '1529974714'),
(716, 118, '_menu_item_type', 'post_type'),
(717, 118, '_menu_item_menu_item_parent', '0'),
(718, 118, '_menu_item_object_id', '72'),
(719, 118, '_menu_item_object', 'projects'),
(720, 118, '_menu_item_target', ''),
(721, 118, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(722, 118, '_menu_item_xfn', ''),
(723, 118, '_menu_item_url', ''),
(724, 118, '_menu_item_orphaned', '1529974715'),
(725, 119, '_menu_item_type', 'post_type'),
(726, 119, '_menu_item_menu_item_parent', '0'),
(727, 119, '_menu_item_object_id', '73'),
(728, 119, '_menu_item_object', 'projects'),
(729, 119, '_menu_item_target', ''),
(730, 119, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(731, 119, '_menu_item_xfn', ''),
(732, 119, '_menu_item_url', ''),
(733, 119, '_menu_item_orphaned', '1529974715'),
(734, 120, '_menu_item_type', 'post_type'),
(735, 120, '_menu_item_menu_item_parent', '0'),
(736, 120, '_menu_item_object_id', '82'),
(737, 120, '_menu_item_object', 'projects'),
(738, 120, '_menu_item_target', ''),
(739, 120, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(740, 120, '_menu_item_xfn', ''),
(741, 120, '_menu_item_url', ''),
(742, 120, '_menu_item_orphaned', '1529974715'),
(743, 121, '_menu_item_type', 'post_type'),
(744, 121, '_menu_item_menu_item_parent', '0'),
(745, 121, '_menu_item_object_id', '53'),
(746, 121, '_menu_item_object', 'projects'),
(747, 121, '_menu_item_target', ''),
(748, 121, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(749, 121, '_menu_item_xfn', ''),
(750, 121, '_menu_item_url', ''),
(751, 121, '_menu_item_orphaned', '1529974716'),
(752, 122, '_menu_item_type', 'post_type'),
(753, 122, '_menu_item_menu_item_parent', '0'),
(754, 122, '_menu_item_object_id', '57'),
(755, 122, '_menu_item_object', 'projects'),
(756, 122, '_menu_item_target', ''),
(757, 122, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(758, 122, '_menu_item_xfn', ''),
(759, 122, '_menu_item_url', ''),
(760, 122, '_menu_item_orphaned', '1529974716'),
(761, 123, '_menu_item_type', 'post_type_archive'),
(762, 123, '_menu_item_menu_item_parent', '0'),
(763, 123, '_menu_item_object_id', '-26'),
(764, 123, '_menu_item_object', 'services'),
(765, 123, '_menu_item_target', ''),
(766, 123, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(767, 123, '_menu_item_xfn', ''),
(768, 123, '_menu_item_url', ''),
(770, 124, '_menu_item_type', 'post_type_archive'),
(771, 124, '_menu_item_menu_item_parent', '0'),
(772, 124, '_menu_item_object_id', '-16'),
(773, 124, '_menu_item_object', 'projects'),
(774, 124, '_menu_item_target', ''),
(775, 124, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(776, 124, '_menu_item_xfn', ''),
(777, 124, '_menu_item_url', ''),
(779, 125, '_menu_item_type', 'post_type_archive'),
(780, 125, '_menu_item_menu_item_parent', '0'),
(781, 125, '_menu_item_object_id', '-32'),
(782, 125, '_menu_item_object', 'careers'),
(783, 125, '_menu_item_target', ''),
(784, 125, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(785, 125, '_menu_item_xfn', ''),
(786, 125, '_menu_item_url', ''),
(788, 126, '_wp_attached_file', '2018/06/event1.jpg'),
(789, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:570;s:6:\"height\";i:305;s:4:\"file\";s:18:\"2018/06/event1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"event1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"event1-300x161.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:161;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"projects\";a:4:{s:4:\"file\";s:18:\"event1-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:18:\"event1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(790, 127, '_edit_last', '1'),
(791, 127, '_edit_lock', '1529983452:1'),
(792, 54, '_thumbnail_id', '126'),
(793, 54, 'description', 'Lorem ipsum dolor sit amet, maiorum phaedrum\r\nmolestiae qui cu, ex vivendo invidunt tincidunt vix,\r\ndicant fabellas per eu. Ex his inermis.'),
(794, 54, '_description', 'field_5b3195558ba82'),
(795, 129, '_wp_attached_file', '2018/06/g1.jpg'),
(796, 129, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g1-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(797, 53, '_thumbnail_id', '129'),
(798, 130, '_edit_last', '1'),
(799, 130, '_edit_lock', '1530055218:1'),
(800, 55, 'description', 'Lorem ipsum dolor sit amet, no noster deleniti mea, decore fierent apeirian an vis. An ius choro deserunt assueverit, atqui \r\ndelicatissimi no duo. Tantas latine pro ex, solet reprimique necessitatibus ut eos. Id autem audire utamur cum. Saperet noluisse quo\r\neu, altera gubergren sed an, vel ex alia saepe legendos. Cu usu eirmod sapientem. Theophrastus concludaturque ei vix, quot'),
(801, 55, '_description', 'field_5b3195558ba82'),
(802, 17, '_wp_page_template', 'page-about.php'),
(803, 17, 'our_mission', ''),
(804, 17, '_our_mission', 'field_5b17beede67a8'),
(805, 17, 'our_vision', ''),
(806, 17, '_our_vision', 'field_5b17bf90af0be'),
(807, 18, 'our_mission', ''),
(808, 18, '_our_mission', 'field_5b17beede67a8'),
(809, 18, 'our_vision', ''),
(810, 18, '_our_vision', 'field_5b17bf90af0be'),
(811, 26, '_edit_lock', '1529987756:1'),
(812, 26, '_edit_last', '1'),
(813, 17, 'who_we_are', 'Ebdaa Engineering Company is an Egyptian registered , formed as a shareholders company on 2014 providing engineering services. We have successful track records in creating many success stories for clients.'),
(814, 17, '_who_we_are', 'field_5b31b2774ccd2'),
(815, 17, 'what_we_do', '<ul>\r\n 	<li>Fully coordination between all trades to provide a General Arrangement (concrete dimension).</li>\r\n 	<li>Rebar shop drawings and bar bending schedule using British standard BS 8666-2005, Egyptian code ECP-205 or any other code required by client, We can provide shop drawings &amp; BBS for all elements of all different type of buildings (high rise buildings, low rise buildings, bridges, tunnels, … etc.).</li>\r\n 	<li>Calculate the percentage of steel for reinforced concrete items in BOQ for the potential projects.</li>\r\n 	<li>Steel structure shop drawings.</li>\r\n 	<li>Arch. shop drawings.</li>\r\n 	<li>MEP shop drawings “Buildings “&amp; Infrastructure “.</li>\r\n 	<li>Quantity Survey for all Elements.</li>\r\n 	<li>Produce 3D Model by using BIM technology to provide accurate technical information for the project to avoid any clashes that may cause project delay.</li>\r\n</ul>'),
(816, 17, '_what_we_do', 'field_5b31b2774ccd1'),
(817, 17, 'why_ebdaa', '<ul>\r\n 	<li>Big projects inside and outside Egypt.</li>\r\n 	<li>Our prices are competitive regarding to our highest level of quality.</li>\r\n 	<li>Reduce the scrap percentage of steel bars.</li>\r\n 	<li>Having our standard presentation of shop drawings, meanwhile we are professional and ﬂexible enough to ﬁt for any required client presentation.</li>\r\n 	<li>Provide highest level of commitment to our clients.</li>\r\n 	<li>Free clashes model.</li>\r\n</ul>'),
(818, 17, '_why_ebdaa', 'field_5b31b2744ccd0'),
(819, 17, 'ebdaa_vission', 'A multiple discipline engineering company is our target which covering each and every trade all over the world'),
(820, 17, '_ebdaa_vission', 'field_5b17beede67a8'),
(821, 17, 'ebdaa_mission', '<ul>\r\n 	<li>Through using our innovative employees, we provide the highest level of commitment to our clients by providing the highest quality, productivity, at proper time with competitive prices, we target the client satisfaction.</li>\r\n 	<li>Our Professional Employees are Our Assets.</li>\r\n</ul>'),
(822, 17, '_ebdaa_mission', 'field_5b17bf90af0be'),
(823, 135, 'our_mission', ''),
(824, 135, '_our_mission', 'field_5b17beede67a8'),
(825, 135, 'our_vision', ''),
(826, 135, '_our_vision', 'field_5b17bf90af0be'),
(827, 135, 'who_we_are', 'Ebdaa Engineering Company is an Egyptian registered , formed as a shareholders company on 2014 providing engineering services. We have successful track records in creating many success stories for clients.'),
(828, 135, '_who_we_are', 'field_5b31b2774ccd2'),
(829, 135, 'what_we_do', '• Fully coordination between all trades to provide a General Arrangement (concrete dimension).\r\n• Rebar shop drawings and bar bending schedule using British standard BS 8666-2005, Egyptian code ECP-205 or any other code required by client, We can provide shop drawings &amp; BBS for all elements of all different type of buildings (high rise buildings, low rise buildings, bridges, tunnels, … etc.).\r\n• Calculate the percentage of steel for reinforced concrete items in BOQ for the potential projects.\r\n• Steel structure shop drawings.\r\n• Arch. shop drawings.\r\n• MEP shop drawings “Buildings “&amp; Infrastructure “.\r\n• Quantity Survey for all Elements.\r\n• Produce 3D Model by using BIM technology to provide accurate technical information for the project to avoid any clashes that may cause project delay.'),
(830, 135, '_what_we_do', 'field_5b31b2774ccd1'),
(831, 135, 'why_ebdaa', '• Big projects inside and outside Egypt.\r\n• Our prices are competitive regarding to our highest level of quality.\r\n• Reduce the scrap percentage of steel bars.\r\n• Having our standard presentation of shop drawings, meanwhile we are professional and ﬂexible enough to ﬁt for any required client presentation.\r\n• Provide highest level of commitment to our clients.\r\n• Free clashes model.'),
(832, 135, '_why_ebdaa', 'field_5b31b2744ccd0'),
(833, 135, 'ebdaa_vission', 'A multiple discipline engineering company is our target which covering each and every trade all over the world'),
(834, 135, '_ebdaa_vission', 'field_5b17beede67a8'),
(835, 135, 'ebdaa_mission', '• Through using our innovative employees, we provide the highest level of commitment to our clients by providing the highest quality, productivity, at proper time with competitive prices, we target the client satisfaction.\r\n• Our Professional Employees are Our Assets.'),
(836, 135, '_ebdaa_mission', 'field_5b17bf90af0be'),
(837, 136, 'our_mission', ''),
(838, 136, '_our_mission', 'field_5b17beede67a8'),
(839, 136, 'our_vision', ''),
(840, 136, '_our_vision', 'field_5b17bf90af0be'),
(841, 136, 'who_we_are', 'Ebdaa Engineering Company is an Egyptian registered , formed as a shareholders company on 2014 providing engineering services. We have successful track records in creating many success stories for clients.'),
(842, 136, '_who_we_are', 'field_5b31b2774ccd2');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(843, 136, 'what_we_do', '<ul>\r\n 	<li>Fully coordination between all trades to provide a General Arrangement (concrete dimension).</li>\r\n 	<li>Rebar shop drawings and bar bending schedule using British standard BS 8666-2005, Egyptian code ECP-205 or any other code required by client, We can provide shop drawings &amp; BBS for all elements of all different type of buildings (high rise buildings, low rise buildings, bridges, tunnels, … etc.).</li>\r\n 	<li>Calculate the percentage of steel for reinforced concrete items in BOQ for the potential projects.</li>\r\n 	<li>Steel structure shop drawings.</li>\r\n 	<li>Arch. shop drawings.</li>\r\n 	<li>MEP shop drawings “Buildings “&amp; Infrastructure “.</li>\r\n 	<li>Quantity Survey for all Elements.</li>\r\n 	<li>Produce 3D Model by using BIM technology to provide accurate technical information for the project to avoid any clashes that may cause project delay.</li>\r\n</ul>'),
(844, 136, '_what_we_do', 'field_5b31b2774ccd1'),
(845, 136, 'why_ebdaa', '<ul>\r\n 	<li>Big projects inside and outside Egypt.</li>\r\n 	<li>Our prices are competitive regarding to our highest level of quality.</li>\r\n 	<li>Reduce the scrap percentage of steel bars.</li>\r\n 	<li>Having our standard presentation of shop drawings, meanwhile we are professional and ﬂexible enough to ﬁt for any required client presentation.</li>\r\n 	<li>Provide highest level of commitment to our clients.</li>\r\n 	<li>Free clashes model.</li>\r\n</ul>'),
(846, 136, '_why_ebdaa', 'field_5b31b2744ccd0'),
(847, 136, 'ebdaa_vission', 'A multiple discipline engineering company is our target which covering each and every trade all over the world'),
(848, 136, '_ebdaa_vission', 'field_5b17beede67a8'),
(849, 136, 'ebdaa_mission', '<ul>\r\n 	<li>Through using our innovative employees, we provide the highest level of commitment to our clients by providing the highest quality, productivity, at proper time with competitive prices, we target the client satisfaction.</li>\r\n 	<li>Our Professional Employees are Our Assets.</li>\r\n</ul>'),
(850, 136, '_ebdaa_mission', 'field_5b17bf90af0be'),
(851, 54, 'featured', '1'),
(852, 54, '_featured', 'field_5b31acc9e35b7'),
(853, 58, 'description', ''),
(854, 58, '_description', 'field_5b3195558ba82'),
(855, 58, 'featured', '1'),
(856, 58, '_featured', 'field_5b31acc9e35b7'),
(857, 53, 'featured', '1'),
(858, 53, '_featured', 'field_5b31acc9e35b7'),
(859, 57, 'featured', '1'),
(860, 57, '_featured', 'field_5b31acc9e35b7'),
(861, 137, '_wp_attached_file', '2018/06/g1-1.jpg'),
(862, 137, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:16:\"2018/06/g1-1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"g1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"g1-1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:16:\"g1-1-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:16:\"g1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(863, 138, '_wp_attached_file', '2018/06/g2.jpg'),
(864, 138, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g2.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g2-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g2-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(865, 139, '_wp_attached_file', '2018/06/g3.jpg'),
(866, 139, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g3.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g3-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g3-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(867, 140, '_wp_attached_file', '2018/06/g4.jpg'),
(868, 140, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g4.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g4-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g4-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(869, 141, '_wp_attached_file', '2018/06/g5.jpg'),
(870, 141, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g5.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g5-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g5-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g5-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(871, 142, '_wp_attached_file', '2018/06/g6.jpg'),
(872, 142, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g6.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g6-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g6-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g6-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(873, 143, '_wp_attached_file', '2018/06/g7.jpg'),
(874, 143, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g7.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g7-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g7-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(875, 144, '_wp_attached_file', '2018/06/g8.jpg'),
(876, 144, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:14:\"2018/06/g8.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"g8-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"g8-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"services\";a:4:{s:4:\"file\";s:14:\"g8-500x305.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:305;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"g8-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(877, 145, '_form', '<div class=\"form-group\"><div class=\"col-sm-12\">\n[text* your-name class:form-control placeholder \"Name\"]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[email* your-email class:form-control placeholder \"Email\"]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[text your-subject class:form-control placeholder \"Phone Number\"]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[file* your-file class:form-control filetypes:pdf|doc|docx|txt limit:2mb]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\nInput this code: [captchac captcha-778 size:l fg:#ffffff bg:#000000][captchar captcha-778 4/4]</div></div>\n<div class=\"form-group\"><div class=\"col-sm-12\">\n[submit \"Send\"]</div></div>'),
(878, 145, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:22:\"EBDAA \"[your-subject]\"\";s:6:\"sender\";s:34:\"[your-name] <m.rohouma1@gmail.com>\";s:9:\"recipient\";s:20:\"m.rohouma1@gmail.com\";s:4:\"body\";s:166:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(879, 145, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:22:\"EBDAA \"[your-subject]\"\";s:6:\"sender\";s:28:\"EBDAA <m.rohouma1@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:108:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\";s:18:\"additional_headers\";s:30:\"Reply-To: m.rohouma1@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(880, 145, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(881, 145, '_additional_settings', ''),
(882, 145, '_locale', 'en_US');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-06-06 10:09:56', '2018-06-06 10:09:56', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-06-06 10:09:56', '2018-06-06 10:09:56', '', 0, 'http://localhost/ebdaa/?p=1', 0, 'post', '', 1),
(2, 1, '2018-06-06 10:09:56', '2018-06-06 10:09:56', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/ebdaa/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2018-06-25 11:33:50', '2018-06-25 11:33:50', '', 0, 'http://localhost/ebdaa/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-06-06 10:09:56', '2018-06-06 10:09:56', '<h2>Who we are</h2><p>Our website address is: http://localhost/ebdaa.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'trash', 'closed', 'open', '', 'privacy-policy__trashed', '', '', '2018-06-19 13:51:01', '2018-06-19 13:51:01', '', 0, 'http://localhost/ebdaa/?page_id=3', 0, 'page', '', 0),
(6, 1, '2018-06-06 10:35:56', '2018-06-06 10:35:56', '', 'Projects', '', 'publish', 'closed', 'closed', '', 'acf_projects', '', '', '2018-06-06 11:23:03', '2018-06-06 11:23:03', '', 0, 'http://localhost/ebdaa/?post_type=acf&#038;p=6', 0, 'acf', '', 0),
(14, 1, '2018-06-06 10:57:12', '2018-06-06 10:57:12', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-06-25 13:47:35', '2018-06-25 13:47:35', '', 0, 'http://localhost/ebdaa/?page_id=14', 0, 'page', '', 0),
(15, 1, '2018-06-06 10:57:12', '2018-06-06 10:57:12', '', 'Home', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2018-06-06 10:57:12', '2018-06-06 10:57:12', '', 14, 'http://localhost/ebdaa/2018/06/06/14-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2018-06-06 10:59:08', '2018-06-06 10:59:08', '', 'Slider', '', 'publish', 'closed', 'closed', '', 'acf_slider', '', '', '2018-06-06 11:02:50', '2018-06-06 11:02:50', '', 0, 'http://localhost/ebdaa/?post_type=acf&#038;p=16', 0, 'acf', '', 0),
(17, 1, '2018-06-06 11:00:43', '2018-06-06 11:00:43', '', 'About us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-06-26 03:55:47', '2018-06-26 03:55:47', '', 0, 'http://localhost/ebdaa/?page_id=17', 0, 'page', '', 0),
(18, 1, '2018-06-06 11:00:43', '2018-06-06 11:00:43', '', 'About us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-06-06 11:00:43', '2018-06-06 11:00:43', '', 17, 'http://localhost/ebdaa/2018/06/06/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2018-06-06 11:01:55', '2018-06-06 11:01:55', '', 'About us', '', 'publish', 'closed', 'closed', '', 'acf_about-us', '', '', '2018-06-06 11:05:12', '2018-06-06 11:05:12', '', 0, 'http://localhost/ebdaa/?post_type=acf&#038;p=19', 0, 'acf', '', 0),
(20, 1, '2018-06-06 11:07:30', '2018-06-06 11:07:30', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2018-06-25 12:39:57', '2018-06-25 12:39:57', '', 0, 'http://localhost/ebdaa/?page_id=20', 0, 'page', '', 0),
(21, 1, '2018-06-06 11:07:30', '2018-06-06 11:07:30', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-06 11:07:30', '2018-06-06 11:07:30', '', 20, 'http://localhost/ebdaa/2018/06/06/20-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2018-06-06 11:15:29', '2018-06-06 11:15:29', '', 'Contact Us', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2018-06-06 11:15:29', '2018-06-06 11:15:29', '', 0, 'http://localhost/ebdaa/?page_id=22', 0, 'page', '', 0),
(23, 1, '2018-06-06 11:10:24', '2018-06-06 11:10:24', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'acf_contact-us', '', '', '2018-06-06 11:27:18', '2018-06-06 11:27:18', '', 0, 'http://localhost/ebdaa/?post_type=acf&#038;p=23', 0, 'acf', '', 0),
(24, 1, '2018-06-06 11:15:29', '2018-06-06 11:15:29', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-06-06 11:15:29', '2018-06-06 11:15:29', '', 22, 'http://localhost/ebdaa/2018/06/06/22-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2018-06-06 11:30:07', '2018-06-06 11:30:07', '<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[text* your-name class:form-control placeholder \"Name\"]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[email* your-email class:form-control placeholder \"Email\"]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[text your-subject class:form-control placeholder \"subject\"]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[textarea your-message class:form-control placeholder \"Your Message\"]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\nInput this code: [captchac captcha-778 size:l fg:#ffffff bg:#000000][captchar captcha-778 4/4]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[submit \"Submit Message\"]</div></div>\n1\nEBDAA \"[your-subject]\"\n[your-name] <m.rohouma1@gmail.com>\nm.rohouma1@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\nReply-To: [your-email]\n\n\n\n\nEBDAA \"[your-subject]\"\nEBDAA <m.rohouma1@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\nReply-To: m.rohouma1@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-06-26 05:09:07', '2018-06-26 05:09:07', '', 0, 'http://localhost/ebdaa/?post_type=wpcf7_contact_form&#038;p=25', 0, 'wpcf7_contact_form', '', 0),
(26, 1, '2018-06-11 09:10:15', '2018-06-11 09:10:15', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"17\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:1:{i:0;s:11:\"the_content\";}s:11:\"description\";s:0:\"\";}', 'About us', 'about-us', 'publish', 'closed', 'closed', '', 'group_5b1e3c770e7be', '', '', '2018-06-26 03:33:25', '2018-06-26 03:33:25', '', 0, 'http://localhost/ebdaa/?post_type=acf-field-group&#038;p=26', 0, 'acf-field-group', '', 0),
(27, 1, '2018-06-11 09:10:15', '2018-06-11 09:10:15', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Ebdaa Vission', 'ebdaa_vission', 'publish', 'closed', 'closed', '', 'field_5b17beede67a8', '', '', '2018-06-26 03:33:25', '2018-06-26 03:33:25', '', 26, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=27', 3, 'acf-field', '', 0),
(28, 1, '2018-06-11 09:10:15', '2018-06-11 09:10:15', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Ebdaa Mission', 'ebdaa_mission', 'publish', 'closed', 'closed', '', 'field_5b17bf90af0be', '', '', '2018-06-26 03:33:25', '2018-06-26 03:33:25', '', 26, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=28', 4, 'acf-field', '', 0),
(29, 1, '2018-06-11 09:10:15', '2018-06-11 09:10:15', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"20\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:1:{i:0;s:11:\"the_content\";}s:11:\"description\";s:0:\"\";}', 'Contact Us', 'contact-us', 'publish', 'closed', 'closed', '', 'group_5b1e3c776254d', '', '', '2018-06-25 12:46:37', '2018-06-25 12:46:37', '', 0, 'http://localhost/ebdaa/?post_type=acf-field-group&#038;p=29', 0, 'acf-field-group', '', 0),
(30, 1, '2018-06-11 09:10:15', '2018-06-11 09:10:15', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"Location\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:4;s:9:\"new_lines\";s:7:\"wpautop\";}', 'Location', 'location', 'publish', 'closed', 'closed', '', 'field_5b17c0f7449eb', '', '', '2018-06-25 12:46:37', '2018-06-25 12:46:37', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=30', 3, 'acf-field', '', 0),
(31, 1, '2018-06-11 09:10:15', '2018-06-11 09:10:15', 'a:9:{s:4:\"type\";s:10:\"google_map\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:10:\"center_lat\";s:0:\"\";s:10:\"center_lng\";s:0:\"\";s:4:\"zoom\";s:0:\"\";s:6:\"height\";s:0:\"\";s:17:\"conditional_logic\";i:0;}', 'Map', 'map', 'publish', 'closed', 'closed', '', 'field_5b17c14514635', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=31', 4, 'acf-field', '', 0),
(34, 1, '2018-06-11 09:10:15', '2018-06-11 09:10:15', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"projects\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Projects', 'projects', 'publish', 'closed', 'closed', '', 'group_5b1e3c77cbf30', '', '', '2018-06-26 02:52:34', '2018-06-26 02:52:34', '', 0, 'http://localhost/ebdaa/?post_type=acf-field-group&#038;p=34', 0, 'acf-field-group', '', 0),
(36, 1, '2018-06-11 09:10:16', '2018-06-11 09:10:16', 'a:16:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"insert\";s:7:\"prepend\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Gallery', 'gallery', 'publish', 'closed', 'closed', '', 'field_5b17b92713aa8', '', '', '2018-06-26 02:52:34', '2018-06-26 02:52:34', '', 34, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=36', 0, 'acf-field', '', 0),
(37, 1, '2018-06-11 09:10:16', '2018-06-11 09:10:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"14\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:1:{i:0;s:11:\"the_content\";}s:11:\"description\";s:0:\"\";}', 'Slider', 'slider', 'publish', 'closed', 'closed', '', 'group_5b1e3c7836c01', '', '', '2018-06-25 13:43:06', '2018-06-25 13:43:06', '', 0, 'http://localhost/ebdaa/?post_type=acf-field-group&#038;p=37', 0, 'acf-field-group', '', 0),
(38, 1, '2018-06-11 09:10:16', '2018-06-11 09:10:16', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:9:\"Add Slide\";}', 'Slider', 'slider', 'publish', 'closed', 'closed', '', 'field_5b17be28571af', '', '', '2018-06-25 13:25:58', '2018-06-25 13:25:58', '', 37, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=38', 0, 'acf-field', '', 0),
(40, 1, '2018-06-19 13:51:01', '2018-06-19 13:51:01', '<h2>Who we are</h2><p>Our website address is: http://localhost/ebdaa.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-06-19 13:51:01', '2018-06-19 13:51:01', '', 3, 'http://localhost/ebdaa/2018/06/19/3-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2018-06-20 10:39:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-06-20 10:39:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=41', 0, 'post', '', 0),
(42, 1, '2018-06-21 10:50:16', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-06-21 10:50:16', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=42', 0, 'post', '', 0),
(43, 1, '2018-06-21 12:42:02', '2018-06-21 12:42:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:9:\"Telephone\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Telephone', 'telephone', 'publish', 'closed', 'closed', '', 'field_5b2b9be31f554', '', '', '2018-06-21 12:42:02', '2018-06-21 12:42:02', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&p=43', 0, 'acf-field', '', 0),
(44, 1, '2018-06-21 12:42:02', '2018-06-21 12:42:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:5:\"Email\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_5b2b9c051f555', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=44', 2, 'acf-field', '', 0),
(45, 1, '2018-06-21 12:42:02', '2018-06-21 12:42:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"Facebook\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Facebook', 'facebook', 'publish', 'closed', 'closed', '', 'field_5b2b9c341f556', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=45', 5, 'acf-field', '', 0),
(46, 1, '2018-06-21 12:42:02', '2018-06-21 12:42:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:7:\"Twitter\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Twitter', 'twitter', 'publish', 'closed', 'closed', '', 'field_5b2b9c4c1f557', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=46', 6, 'acf-field', '', 0),
(47, 1, '2018-06-21 12:42:02', '2018-06-21 12:42:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:9:\"Linked In\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Linked In', 'linked_in', 'publish', 'closed', 'closed', '', 'field_5b2b9c721f558', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=47', 7, 'acf-field', '', 0),
(48, 1, '2018-06-21 12:42:02', '2018-06-21 12:42:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:7:\"Google+\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Google+', 'google', 'publish', 'closed', 'closed', '', 'field_5b2b9c981f559', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=48', 8, 'acf-field', '', 0),
(49, 1, '2018-06-21 12:42:02', '2018-06-21 12:42:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:7:\"Youtube\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Youtube', 'youtube', 'publish', 'closed', 'closed', '', 'field_5b2b9cd31f55a', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=49', 9, 'acf-field', '', 0),
(50, 1, '2018-06-21 12:43:39', '2018-06-21 12:43:39', ' ', '', '', 'publish', 'closed', 'closed', '', '50', '', '', '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 0, 'http://localhost/ebdaa/?p=50', 7, 'nav_menu_item', '', 0),
(51, 1, '2018-06-21 12:43:40', '2018-06-21 12:43:40', ' ', '', '', 'publish', 'closed', 'closed', '', '51', '', '', '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 0, 'http://localhost/ebdaa/?p=51', 6, 'nav_menu_item', '', 0),
(52, 1, '2018-06-21 12:43:41', '2018-06-21 12:43:41', ' ', '', '', 'publish', 'closed', 'closed', '', '52', '', '', '2018-06-26 01:00:28', '2018-06-26 01:00:28', '', 0, 'http://localhost/ebdaa/?p=52', 1, 'nav_menu_item', '', 0),
(53, 1, '2018-06-21 12:45:49', '2018-06-21 12:45:49', 'Lorem ipsum dolor sit amet, no noster deleniti mea, decore fierent apeirian an vis. An ius choro deserunt assueverit, atqui delicatissimi no duo. Tantas latine pro ex, solet reprimique necessitatibus ut eos. Id autem audire utamur cum. Saperet noluisse quo eu, altera gubergren sed an, vel ex alia saepe legendos. Cu usu eirmod sapientem. Theophrastus concludaturque ei vix, quot ornatus nostrum nec id. Ne efficiendi constituam eos, vel velit equidem ex, usu nostrum percipitur suscipiantur id. Eleifend liberavisse nam no, cu quo partiendo efficiantur.\r\n\r\nLorem ipsum dolor sit amet, no noster deleniti mea, decore fierent apeirian an vis. An ius choro deserunt assueverit, atqui delicatissimi no duo. Tantas latine pro ex, solet reprimique necessitatibus ut eos. Id autem audire utamur cum. Saperet noluisse quo eu, altera gubergren sed an, vel ex alia saepe legendos. Cu usu eirmod sapientem. Theophrastus concludaturque ei vix, quot ornatus nostrum nec id.', 'Project One', '', 'publish', 'closed', 'closed', '', 'project-one', '', '', '2018-06-27 00:06:41', '2018-06-27 00:06:41', '', 0, 'http://localhost/ebdaa/?post_type=projects&#038;p=53', 0, 'projects', '', 0),
(54, 1, '2018-06-21 12:46:05', '2018-06-21 12:46:05', 'Lorem ipsum dolor sit amet, ea est iusto albucius, an vis case natum assueverit, an dico alia sea. Te quo ubique putent scripta. Pri in iudico dicunt philosophia, an lorem fastidii phaedrum his. Iusto torquatos te mei. Aeque iudicabit complectitur qui et, persius quaeque apeirian id pri. Has id virtute consetetur, ne his velit euripidis, an vel nibh expetendis repudiandae.\r\n\r\nEuripidis vulputate at ius, id sonet patrioque contentiones eum. Falli paulo pericula pri ei. Alienum vituperatoribus mel ex, possit audiam at pro. Ne sanctus dissentiunt per, et sea hinc aeterno assentior.\r\n\r\nNec at altera suscipit, etiam causae sit ex. Ut eam mazim legendos, graece debitis ut mel. Habeo omittam at quo. An ferri phaedrum reprehendunt eum, te sit dico volumus appetere, nisl velit an sed. Sed ex interesset appellantur, postulant neglegentur mel an.', 'Service One', '', 'publish', 'closed', 'closed', '', 'service-one', '', '', '2018-06-26 23:34:33', '2018-06-26 23:34:33', '', 0, 'http://localhost/ebdaa/?post_type=services&#038;p=54', 0, 'services', '', 0),
(55, 1, '2018-06-21 12:46:18', '2018-06-21 12:46:18', 'Lorem ipsum dolor sit amet, no noster deleniti mea, decore fierent apeirian an vis. An ius choro deserunt assueverit, atqui delicatissimi no duo. Tantas latine pro ex, solet reprimique necessitatibus ut eos. Id autem audire utamur cum. Saperet noluisse quo eu, altera gubergren sed an, vel ex alia saepe legendos. Cu usu eirmod sapientem. Theophrastus concludaturque ei vix, quot ornatus nostrum nec id. Ne efficiendi constituam eos, vel velit equidem ex, usu nostrum percipitur suscipiantur id. Eleifend liberavisse nam no, cu quo partiendo efficiantur.\r\n\r\nLorem ipsum dolor sit amet, no noster deleniti mea, decore fierent apeirian an vis. An ius choro deserunt assueverit, atqui delicatissimi no duo. Tantas latine pro ex, solet reprimique necessitatibus ut eos. Id autem audire utamur cum. Saperet noluisse quo eu, altera gubergren sed an, vel ex alia saepe legendos. Cu usu eirmod sapientem. Theophrastus concludaturque ei vix, quot ornatus nostrum nec id.', 'Job One', '', 'publish', 'closed', 'closed', '', 'job-one', '', '', '2018-06-27 00:29:40', '2018-06-27 00:29:40', '', 0, 'http://localhost/ebdaa/?post_type=careers&#038;p=55', 0, 'careers', '', 0),
(56, 1, '2018-06-21 12:46:44', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-21 12:46:44', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=56', 1, 'nav_menu_item', '', 0),
(57, 1, '2018-06-21 12:49:11', '2018-06-21 12:49:11', '', 'Project Two', '', 'publish', 'closed', 'closed', '', 'project-two', '', '', '2018-06-26 05:31:52', '2018-06-26 05:31:52', '', 0, 'http://localhost/ebdaa/?post_type=projects&#038;p=57', 0, 'projects', '', 0),
(58, 1, '2018-06-21 12:49:39', '2018-06-21 12:49:39', '', 'Service Two', '', 'publish', 'closed', 'closed', '', 'service-two', '', '', '2018-06-26 05:24:54', '2018-06-26 05:24:54', '', 0, 'http://localhost/ebdaa/?post_type=services&#038;p=58', 0, 'services', '', 0),
(59, 1, '2018-06-21 12:49:55', '2018-06-21 12:49:55', '', 'Job Two', '', 'publish', 'closed', 'closed', '', 'job-two', '', '', '2018-06-21 12:49:55', '2018-06-21 12:49:55', '', 0, 'http://localhost/ebdaa/?post_type=careers&#038;p=59', 0, 'careers', '', 0),
(60, 1, '2018-06-21 12:50:13', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-21 12:50:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=60', 1, 'nav_menu_item', '', 0),
(61, 1, '2018-06-21 12:50:13', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-21 12:50:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=61', 1, 'nav_menu_item', '', 0),
(62, 1, '2018-06-21 12:51:10', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-21 12:51:10', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=62', 1, 'nav_menu_item', '', 0),
(63, 1, '2018-06-21 12:51:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-21 12:51:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=63', 1, 'nav_menu_item', '', 0),
(64, 1, '2018-06-25 10:45:37', '2018-06-25 10:45:37', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-25 10:45:37', '2018-06-25 10:45:37', '', 20, 'http://localhost/ebdaa/2018/06/25/20-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2018-06-25 10:49:16', '2018-06-25 10:49:16', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-25 10:49:16', '2018-06-25 10:49:16', '', 20, 'http://localhost/ebdaa/2018/06/25/20-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2018-06-25 10:53:04', '2018-06-25 10:53:04', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-25 10:53:04', '2018-06-25 10:53:04', '', 20, 'http://localhost/ebdaa/2018/06/25/20-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2018-06-25 10:53:17', '2018-06-25 10:53:17', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-25 10:53:17', '2018-06-25 10:53:17', '', 20, 'http://localhost/ebdaa/2018/06/25/20-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-06-25 11:25:30', '0000-00-00 00:00:00', '', 'Services', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:25:30', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=68', 1, 'nav_menu_item', '', 0),
(69, 1, '2018-06-25 11:25:47', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:25:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=69', 1, 'nav_menu_item', '', 0),
(70, 1, '2018-06-25 11:25:48', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:25:48', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=70', 1, 'nav_menu_item', '', 0),
(71, 1, '2018-06-25 11:26:30', '2018-06-25 11:26:30', '', 'Project 3', '', 'publish', 'closed', 'closed', '', 'project-3', '', '', '2018-06-25 11:26:30', '2018-06-25 11:26:30', '', 0, 'http://localhost/ebdaa/?post_type=projects&#038;p=71', 0, 'projects', '', 0),
(72, 1, '2018-06-25 11:26:40', '2018-06-25 11:26:40', '', 'Project 4', '', 'publish', 'closed', 'closed', '', 'project-4', '', '', '2018-06-25 11:26:40', '2018-06-25 11:26:40', '', 0, 'http://localhost/ebdaa/?post_type=projects&#038;p=72', 0, 'projects', '', 0),
(73, 1, '2018-06-25 11:26:48', '2018-06-25 11:26:48', '', 'Project 5', '', 'publish', 'closed', 'closed', '', 'project-5', '', '', '2018-06-25 11:26:48', '2018-06-25 11:26:48', '', 0, 'http://localhost/ebdaa/?post_type=projects&#038;p=73', 0, 'projects', '', 0),
(77, 1, '2018-06-25 11:27:45', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:27:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=77', 1, 'nav_menu_item', '', 0),
(78, 1, '2018-06-25 11:27:45', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:27:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=78', 1, 'nav_menu_item', '', 0),
(79, 1, '2018-06-25 11:27:46', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:27:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=79', 1, 'nav_menu_item', '', 0),
(80, 1, '2018-06-25 11:27:47', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:27:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=80', 1, 'nav_menu_item', '', 0),
(81, 1, '2018-06-25 11:27:47', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:27:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=81', 1, 'nav_menu_item', '', 0),
(82, 1, '2018-06-25 11:28:40', '2018-06-25 11:28:40', '', 'Project 6', '', 'publish', 'closed', 'closed', '', 'project-6', '', '', '2018-06-25 11:28:40', '2018-06-25 11:28:40', '', 0, 'http://localhost/ebdaa/?post_type=projects&#038;p=82', 0, 'projects', '', 0),
(83, 1, '2018-06-25 11:28:53', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:28:53', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=83', 1, 'nav_menu_item', '', 0),
(84, 1, '2018-06-25 11:28:54', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:28:54', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=84', 1, 'nav_menu_item', '', 0),
(85, 1, '2018-06-25 11:28:54', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:28:54', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=85', 1, 'nav_menu_item', '', 0),
(86, 1, '2018-06-25 11:28:55', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:28:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=86', 1, 'nav_menu_item', '', 0),
(87, 1, '2018-06-25 11:28:56', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:28:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=87', 1, 'nav_menu_item', '', 0),
(88, 1, '2018-06-25 11:28:56', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-25 11:28:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=88', 1, 'nav_menu_item', '', 0),
(91, 1, '2018-06-25 11:33:50', '2018-06-25 11:33:50', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/ebdaa/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-25 11:33:50', '2018-06-25 11:33:50', '', 2, 'http://localhost/ebdaa/2018/06/25/2-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2018-06-25 11:39:04', '2018-06-25 11:39:04', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 0, 'http://localhost/ebdaa/?p=92', 4, 'nav_menu_item', '', 0),
(93, 1, '2018-06-25 11:39:03', '2018-06-25 11:39:03', ' ', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 0, 'http://localhost/ebdaa/?p=93', 3, 'nav_menu_item', '', 0),
(94, 1, '2018-06-25 12:16:27', '2018-06-25 12:16:27', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:4:\".pdf\";}', 'Brochure', 'brochure', 'publish', 'closed', 'closed', '', 'field_5b30dca674b12', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=94', 10, 'acf-field', '', 0),
(95, 1, '2018-06-25 12:16:56', '2018-06-25 12:16:56', '', 'pro', '', 'inherit', 'open', 'closed', '', 'pro', '', '', '2018-06-25 12:16:56', '2018-06-25 12:16:56', '', 20, 'http://localhost/ebdaa/wp-content/uploads/2018/06/pro.pdf', 0, 'attachment', 'application/pdf', 0),
(96, 1, '2018-06-25 12:17:03', '2018-06-25 12:17:03', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-25 12:17:03', '2018-06-25 12:17:03', '', 20, 'http://localhost/ebdaa/2018/06/25/20-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2018-06-25 12:37:26', '2018-06-25 12:37:26', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:9:\"Telephone\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Telephone2', 'telephone2', 'publish', 'closed', 'closed', '', 'field_5b30e1e9c674c', '', '', '2018-06-25 12:37:26', '2018-06-25 12:37:26', '', 29, 'http://localhost/ebdaa/?post_type=acf-field&p=97', 1, 'acf-field', '', 0),
(98, 1, '2018-06-25 12:38:05', '2018-06-25 12:38:05', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-25 12:38:05', '2018-06-25 12:38:05', '', 20, 'http://localhost/ebdaa/2018/06/25/20-revision-v1/', 0, 'revision', '', 0),
(99, 1, '2018-06-25 12:39:57', '2018-06-25 12:39:57', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-06-25 12:39:57', '2018-06-25 12:39:57', '', 20, 'http://localhost/ebdaa/2018/06/25/20-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2018-06-25 13:22:07', '2018-06-25 13:22:07', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5b30ebf34753d', '', '', '2018-06-25 13:43:06', '2018-06-25 13:43:06', '', 38, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=100', 0, 'acf-field', '', 0),
(101, 1, '2018-06-25 13:22:07', '2018-06-25 13:22:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:5:\"Title\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_5b30ec184753e', '', '', '2018-06-25 13:22:07', '2018-06-25 13:22:07', '', 38, 'http://localhost/ebdaa/?post_type=acf-field&p=101', 1, 'acf-field', '', 0),
(102, 1, '2018-06-25 13:22:07', '2018-06-25 13:22:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:9:\"Sub Title\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Sub Title', 'sub_title', 'publish', 'closed', 'closed', '', 'field_5b30ec2d4753f', '', '', '2018-06-25 13:22:07', '2018-06-25 13:22:07', '', 38, 'http://localhost/ebdaa/?post_type=acf-field&p=102', 2, 'acf-field', '', 0),
(103, 1, '2018-06-25 13:22:07', '2018-06-25 13:22:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:4:\"Link\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_5b30ec4347540', '', '', '2018-06-25 13:25:35', '2018-06-25 13:25:35', '', 38, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=103', 4, 'acf-field', '', 0),
(104, 1, '2018-06-25 13:22:07', '2018-06-25 13:22:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:9:\"Link Text\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Link Text', 'link_text', 'publish', 'closed', 'closed', '', 'field_5b30ec5747541', '', '', '2018-06-25 13:25:34', '2018-06-25 13:25:34', '', 38, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=104', 3, 'acf-field', '', 0),
(105, 1, '2018-06-25 13:25:04', '2018-06-25 13:25:04', '', 'slider1', '', 'inherit', 'open', 'closed', '', 'slider1', '', '', '2018-06-25 13:25:06', '2018-06-25 13:25:06', '', 14, 'http://localhost/ebdaa/wp-content/uploads/2018/06/slider1.jpg', 0, 'attachment', 'image/jpeg', 0),
(106, 1, '2018-06-25 13:25:08', '2018-06-25 13:25:08', '', 'Home', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2018-06-25 13:25:08', '2018-06-25 13:25:08', '', 14, 'http://localhost/ebdaa/14-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2018-06-25 13:26:14', '2018-06-25 13:26:14', '', 'slider2', '', 'inherit', 'open', 'closed', '', 'slider2', '', '', '2018-06-25 13:26:16', '2018-06-25 13:26:16', '', 14, 'http://localhost/ebdaa/wp-content/uploads/2018/06/slider2.jpg', 0, 'attachment', 'image/jpeg', 0),
(108, 1, '2018-06-25 13:27:03', '2018-06-25 13:27:03', '', 'Home', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2018-06-25 13:27:03', '2018-06-25 13:27:03', '', 14, 'http://localhost/ebdaa/14-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2018-06-25 13:47:35', '2018-06-25 13:47:35', '', 'Home', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2018-06-25 13:47:35', '2018-06-25 13:47:35', '', 14, 'http://localhost/ebdaa/14-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2018-06-26 00:58:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=110', 1, 'nav_menu_item', '', 0),
(111, 1, '2018-06-26 00:58:11', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=111', 1, 'nav_menu_item', '', 0),
(112, 1, '2018-06-26 00:58:12', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:12', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=112', 1, 'nav_menu_item', '', 0),
(113, 1, '2018-06-26 00:58:12', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:12', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=113', 1, 'nav_menu_item', '', 0),
(114, 1, '2018-06-26 00:58:13', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=114', 1, 'nav_menu_item', '', 0),
(115, 1, '2018-06-26 00:58:13', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=115', 1, 'nav_menu_item', '', 0),
(116, 1, '2018-06-26 00:58:33', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=116', 1, 'nav_menu_item', '', 0),
(117, 1, '2018-06-26 00:58:33', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=117', 1, 'nav_menu_item', '', 0),
(118, 1, '2018-06-26 00:58:34', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:34', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=118', 1, 'nav_menu_item', '', 0),
(119, 1, '2018-06-26 00:58:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=119', 1, 'nav_menu_item', '', 0),
(120, 1, '2018-06-26 00:58:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=120', 1, 'nav_menu_item', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(121, 1, '2018-06-26 00:58:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=121', 1, 'nav_menu_item', '', 0),
(122, 1, '2018-06-26 00:58:36', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-06-26 00:58:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/ebdaa/?p=122', 1, 'nav_menu_item', '', 0),
(123, 1, '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 'Services', '', 'publish', 'closed', 'closed', '', 'services-2', '', '', '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 0, 'http://localhost/ebdaa/?p=123', 2, 'nav_menu_item', '', 0),
(124, 1, '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 'Projects', '', 'publish', 'closed', 'closed', '', 'projects-2', '', '', '2018-06-26 01:00:29', '2018-06-26 01:00:29', '', 0, 'http://localhost/ebdaa/?p=124', 5, 'nav_menu_item', '', 0),
(125, 1, '2018-06-26 01:00:30', '2018-06-26 01:00:30', '', 'Careers', '', 'publish', 'closed', 'closed', '', 'careers-2', '', '', '2018-06-26 01:00:30', '2018-06-26 01:00:30', '', 0, 'http://localhost/ebdaa/?p=125', 8, 'nav_menu_item', '', 0),
(126, 1, '2018-06-26 01:18:03', '2018-06-26 01:18:03', '', 'event1', '', 'inherit', 'open', 'closed', '', 'event1', '', '', '2018-06-26 01:18:03', '2018-06-26 01:18:03', '', 54, 'http://localhost/ebdaa/wp-content/uploads/2018/06/event1.jpg', 0, 'attachment', 'image/jpeg', 0),
(127, 1, '2018-06-26 01:23:35', '2018-06-26 01:23:35', 'a:7:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"services\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"careers\";}}}s:8:\"position\";s:15:\"acf_after_title\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'group_5b3194e104b5a', '', '', '2018-06-26 03:16:10', '2018-06-26 03:16:10', '', 0, 'http://localhost/ebdaa/?post_type=acf-field-group&#038;p=127', 0, 'acf-field-group', '', 0),
(128, 1, '2018-06-26 01:23:35', '2018-06-26 01:23:35', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'field_5b3195558ba82', '', '', '2018-06-26 03:16:10', '2018-06-26 03:16:10', '', 127, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=128', 0, 'acf-field', '', 0),
(129, 1, '2018-06-26 02:51:41', '2018-06-26 02:51:41', '', 'g1', '', 'inherit', 'open', 'closed', '', 'g1', '', '', '2018-06-26 02:51:41', '2018-06-26 02:51:41', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g1.jpg', 0, 'attachment', 'image/jpeg', 0),
(130, 1, '2018-06-26 03:04:11', '2018-06-26 03:04:11', 'a:7:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"projects\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"services\";}}}s:8:\"position\";s:4:\"side\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Featured', 'featured', 'publish', 'closed', 'closed', '', 'group_5b31acbde098f', '', '', '2018-06-26 03:04:43', '2018-06-26 03:04:43', '', 0, 'http://localhost/ebdaa/?post_type=acf-field-group&#038;p=130', 0, 'acf-field-group', '', 0),
(131, 1, '2018-06-26 03:04:11', '2018-06-26 03:04:11', 'a:10:{s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"message\";s:0:\"\";s:13:\"default_value\";i:0;s:2:\"ui\";i:0;s:10:\"ui_on_text\";s:0:\"\";s:11:\"ui_off_text\";s:0:\"\";}', 'Featured', 'featured', 'publish', 'closed', 'closed', '', 'field_5b31acc9e35b7', '', '', '2018-06-26 03:04:11', '2018-06-26 03:04:11', '', 130, 'http://localhost/ebdaa/?post_type=acf-field&p=131', 0, 'acf-field', '', 0),
(132, 1, '2018-06-26 03:30:40', '2018-06-26 03:30:40', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Who We Are', 'who_we_are', 'publish', 'closed', 'closed', '', 'field_5b31b2774ccd2', '', '', '2018-06-26 03:33:25', '2018-06-26 03:33:25', '', 26, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=132', 0, 'acf-field', '', 0),
(133, 1, '2018-06-26 03:30:40', '2018-06-26 03:30:40', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'What We Do', 'what_we_do', 'publish', 'closed', 'closed', '', 'field_5b31b2774ccd1', '', '', '2018-06-26 03:33:25', '2018-06-26 03:33:25', '', 26, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=133', 1, 'acf-field', '', 0),
(134, 1, '2018-06-26 03:30:41', '2018-06-26 03:30:41', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Why Ebdaa', 'why_ebdaa', 'publish', 'closed', 'closed', '', 'field_5b31b2744ccd0', '', '', '2018-06-26 03:33:25', '2018-06-26 03:33:25', '', 26, 'http://localhost/ebdaa/?post_type=acf-field&#038;p=134', 2, 'acf-field', '', 0),
(135, 1, '2018-06-26 03:35:53', '2018-06-26 03:35:53', '', 'About us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-06-26 03:35:53', '2018-06-26 03:35:53', '', 17, 'http://localhost/ebdaa/17-revision-v1/', 0, 'revision', '', 0),
(136, 1, '2018-06-26 03:55:47', '2018-06-26 03:55:47', '', 'About us', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-06-26 03:55:47', '2018-06-26 03:55:47', '', 17, 'http://localhost/ebdaa/17-revision-v1/', 0, 'revision', '', 0),
(137, 1, '2018-06-27 00:06:29', '2018-06-27 00:06:29', '', 'g1', '', 'inherit', 'open', 'closed', '', 'g1-2', '', '', '2018-06-27 00:06:36', '2018-06-27 00:06:36', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2018-06-27 00:06:30', '2018-06-27 00:06:30', '', 'g2', '', 'inherit', 'open', 'closed', '', 'g2', '', '', '2018-06-27 00:06:30', '2018-06-27 00:06:30', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g2.jpg', 0, 'attachment', 'image/jpeg', 0),
(139, 1, '2018-06-27 00:06:31', '2018-06-27 00:06:31', '', 'g3', '', 'inherit', 'open', 'closed', '', 'g3', '', '', '2018-06-27 00:06:31', '2018-06-27 00:06:31', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g3.jpg', 0, 'attachment', 'image/jpeg', 0),
(140, 1, '2018-06-27 00:06:31', '2018-06-27 00:06:31', '', 'g4', '', 'inherit', 'open', 'closed', '', 'g4', '', '', '2018-06-27 00:06:31', '2018-06-27 00:06:31', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g4.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 1, '2018-06-27 00:06:32', '2018-06-27 00:06:32', '', 'g5', '', 'inherit', 'open', 'closed', '', 'g5', '', '', '2018-06-27 00:06:32', '2018-06-27 00:06:32', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g5.jpg', 0, 'attachment', 'image/jpeg', 0),
(142, 1, '2018-06-27 00:06:33', '2018-06-27 00:06:33', '', 'g6', '', 'inherit', 'open', 'closed', '', 'g6', '', '', '2018-06-27 00:06:33', '2018-06-27 00:06:33', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g6.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2018-06-27 00:06:34', '2018-06-27 00:06:34', '', 'g7', '', 'inherit', 'open', 'closed', '', 'g7', '', '', '2018-06-27 00:06:34', '2018-06-27 00:06:34', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g7.jpg', 0, 'attachment', 'image/jpeg', 0),
(144, 1, '2018-06-27 00:06:35', '2018-06-27 00:06:35', '', 'g8', '', 'inherit', 'open', 'closed', '', 'g8', '', '', '2018-06-27 00:06:35', '2018-06-27 00:06:35', '', 53, 'http://localhost/ebdaa/wp-content/uploads/2018/06/g8.jpg', 0, 'attachment', 'image/jpeg', 0),
(145, 1, '2018-06-27 00:32:34', '2018-06-27 00:32:34', '<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[text* your-name class:form-control placeholder \"Name\"]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[email* your-email class:form-control placeholder \"Email\"]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[text your-subject class:form-control placeholder \"Phone Number\"]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[file* your-file class:form-control filetypes:pdf|doc|docx|txt limit:2mb]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\nInput this code: [captchac captcha-778 size:l fg:#ffffff bg:#000000][captchar captcha-778 4/4]</div></div>\r\n<div class=\"form-group\"><div class=\"col-sm-12\">\r\n[submit \"Send\"]</div></div>\n1\nEBDAA \"[your-subject]\"\n[your-name] <m.rohouma1@gmail.com>\nm.rohouma1@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\nReply-To: [your-email]\n\n\n\n\nEBDAA \"[your-subject]\"\nEBDAA <m.rohouma1@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on EBDAA (http://localhost/ebdaa)\nReply-To: m.rohouma1@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Careers form', '', 'publish', 'closed', 'closed', '', 'contact-form_copy', '', '', '2018-06-27 00:48:48', '2018-06-27 00:48:48', '', 0, 'http://localhost/ebdaa/?post_type=wpcf7_contact_form&#038;p=145', 0, 'wpcf7_contact_form', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'menu', 'menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(50, 2, 0),
(51, 2, 0),
(52, 2, 0),
(92, 2, 0),
(93, 2, 0),
(123, 2, 0),
(124, 2, 0),
(125, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"8d514d91f366d99919d2aac62d63dc9f53f08dcd11748980bc012475257aabf6\";a:4:{s:10:\"expiration\";i:1530096084;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36\";s:5:\"login\";i:1529923284;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '41'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(20, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce'),
(23, 1, 'wp_user-settings-time', '1530056069'),
(24, 1, 'acf_user_settings', 'a:0:{}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BvJoEaYjnLc.e.IArai/h/jWFQR.Wb/', 'admin', 'm.rohouma1@gmail.com', '', '2018-06-06 10:09:55', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=342;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=883;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
